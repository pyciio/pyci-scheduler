.PHONY: env tests

VIRTUALENV := $(shell command -v virtualenv --version 2> /dev/null)
VENV_DIR = venv
OS_TYPE := $(shell uname -s)
VERSION = $(shell cat ./VERSION)
APP_ROOT = /opt/pyci-scheduler
# Package settings
PKG_BUILD_DIR = ./dist
PKG_CONTENT = keys=${APP_ROOT} \
              rest=${APP_ROOT} \
              scheduler=${APP_ROOT} \
              db_repository=${APP_ROOT} \
              scheduler.py=${APP_ROOT}/scheduler.py \
              config.py=${APP_ROOT}/config.py \
              requirements.txt=${APP_ROOT}/requirements.txt \
              scripts/db_manage.py=${APP_ROOT}/scripts/db_manage.py \
              scripts/pyci-scheduler=/etc/init.d/pyci-scheduler \
              scripts/pyci-scheduler-api=/etc/init.d/pyci-scheduler-api
# pakage name = git project name
PKG_NAME = pyci-scheduler
PKG_INFO = -a all --url $(shell git config --get remote.origin.url) \
           --category "python" \
           --license GPL 
PKG_DESCR = --description "PyCI scheduler with RESTful API server"
PKG_POST_INSTALL = ./scripts/post_install_script.sh
PKG_CONF = --config-files  --config-files etc/portal-wsgi/hist.conf
PKG_DEPENDENS = python-virtualenv -d python-dev -d python3-dev

clean:
	-rm -rf ${PKG_BUILD_DIR}
	-rm -rf ./venv
	-rm -rf ./.tox
	find . -name "*.pyo" -delete
	find . -name "*.pyc" -delete
	find . -type d -name "__pycache__" -delete

package-ubuntu: clean
	fpm -t deb -s dir -v ${VERSION} \
		-n ${PKG_NAME} \
		-d ${PKG_DEPENDENS} \
		--after-install ${PKG_POST_INSTALL} \
		--deb-no-default-config-files \
		${PKG_INFO} \
		${PKG_DESCR} \
		${PKG_CONTENT} 
	mkdir -p ${PKG_BUILD_DIR}
	mv ./*.deb ${PKG_BUILD_DIR}

package-rpm: clean
	fpm -t rpm -s dir -v ${VERSION} \
		-n ${PKG_NAME} \
		-d ${PKG_DEPENDENS} \
		--after-install ${PKG_POST_INSTALL} \
		${PKG_INFO} \
		${PKG_DESCR} \
		${PKG_CONTENT} 
	mkdir -p ${PKG_BUILD_DIR}
	mv ./*.rpm ${PKG_BUILD_DIR}

packages: package-ubuntu package-rpm

env: ${VENV_DIR}
	@echo "OS requirements: python-dev python3-dev"
ifndef VIRTUALENV
	$(error "command 'virtualenv' is not available! Please install python-virtualenv")
	exit 1
endif
	@echo "Install env for python2...."
	@virtualenv -p python2 ${VENV_DIR}/py2
	${VENV_DIR}/py2/bin/pip install -r requirements.txt
	@echo "Install env for python2...."
	@virtualenv -p python3 ${VENV_DIR}/py3
	${VENV_DIR}/py3/bin/pip install -r requirements.txt

tests:
	@tox

lint:
	@tox -e lint

coverage:
	@tox -e coverage

coverage-report: coverage
	./.tox/coverage/bin/coverage html
ifeq ($(OS_TYPE),Linux)
	xdg-open /tmp/pyci-htmlcov/index.html
endif
ifeq ($(OS_TYPE),Darwin)
	open /tmp/pyci-htmlcov/index.html
endif

${VENV_DIR}:
	mkdir -p $@

help:
	@echo "    clean"
	@echo "        Remove build artifacts and packages."
	@echo "    env"
	@echo "        Rebuild virtualenv and install requirements."
	@echo '    package-ubuntu'
	@echo '        Build DEB packages.'
	@echo '    package-rpm'
	@echo '        Build RPM packages.'
	@echo '    packages'
	@echo '        Build all types packages.'
	@echo '    build-image'
	@echo '        Build docker image.'
	@echo '    lint'
	@echo '        Run code linting.'
	@echo '    tests'
	@echo '        Run unittests.'
	@echo '    coverage'
	@echo '        Calculate unittests coverage.'
	@echo '    coverage-report'
	@echo '        Calculate unittests coverage and open HTML report'

