#!/usr/bin/env python
"""
Script for run application
"""
import os
import sys
import logging
import pwd
import grp
import argparse
from scheduler import Scheduler  # pylint: disable=import-self

LOG = logging.getLogger('pyci-scheduler')


def log_lvl(level):
    """
    Check and setup logging level
    """
    try:
        LOG.setLevel(logging.getLevelName(level.upper()))
    except ValueError:
        raise argparse.ArgumentTypeError("bad log level '{0}'".format(level))


def get_options():
    """
    Command line argument parser
    """
    user = pwd.getpwuid(os.geteuid()).pw_name
    group = grp.getgrgid(os.getegid()).gr_name
    parser = argparse.ArgumentParser(prog='PyCI scheduler',
                                     usage="usage: scheduler.py [OPTIONS]",
                                     add_help=False)
    parser.add_argument('-h', '--help', action='help',
                        help="- Print the help message")
    parser.add_argument('-D', '--daemon', action='store_true', default=False,
                        help="- Daemonize the Scheduler process. [False]")
    parser.add_argument('-l', '--log', type=str, default='-',
                        help="- The log file to write to. [-]")
    parser.add_argument('--log-level', type=log_lvl, default='info',
                        help="- The granularity of log outputs. [info]")
    parser.add_argument('-p', '--pid', type=str,
                        help="- A filename to use for the PID file. [None]")
    parser.add_argument('-u', '--user', type=str, default=user,
                        help="- Switch worker processes to run as this user. "
                        "[{0}]".format(user))
    parser.add_argument('-g', '--group', type=str, default=group,
                        help="- Switch worker process to run as this group. "
                        "[{0}]".format(group))
    parser.add_argument('--max-workers', type=int,
                        help="- Max count parallel workers. [None]")
    try:
        opts = parser.parse_args()
        if opts.daemon and opts.pid is None:
            parser.error("'--daemon' option depends on '--pid' option")
        if opts.pid is not None and not opts.daemon:
            parser.error("'--pid' option depends on '--daemon' option")
        return opts
    except SystemExit:
        exit(1)


# pylint: disable=invalid-name
if __name__ == "__main__":
    options = get_options()

    if options.log == "-":
        stdout_hdlr = logging.StreamHandler(sys.stdout)
        stdout_hdlr.setLevel(logging.DEBUG)
        LOG.addHandler(stdout_hdlr)
    else:
        file_hdlr = logging.FileHandler(options.log_file, "w+")
        file_hdlr.setLevel(logging.DEBUG)
        LOG.addHandler(file_hdlr)
    # Run scheduler
    scheduler = Scheduler()
    if options.daemon:
        scheduler.run_as_daemon(options.user, options.group, options.pid)
    else:
        scheduler.run()
