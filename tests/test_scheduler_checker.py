"""
Tests for module scheduler.checker
"""

try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock
import unittest
import six
import git
import yaml
from scheduler.checker import ProjectChecker


# pylint: disable=protected-access
class TestProjectChecker(unittest.TestCase):
    """Testing class scheduler.checker.ProjectChecker"""

    def setUp(self):
        # mocking db_engine
        self.mock_engine = mock.Mock()

    def test_init(self):
        """Test ProjectChecker.__init__()"""
        ProjectChecker(self.mock_engine, None, None)
        # check db_engine_.dispose()
        self.assertTrue(self.mock_engine.dispose.called)

    # sqlalchemy.orm.sessionmaker
    @mock.patch('scheduler.checker.sessionmaker')
    def test_context(self, mock_maker):
        """Test ProjectChecker context"""
        mock_sess = None
        with ProjectChecker(self.mock_engine, 1, None) as checker:
            # Test create database session
            mock_maker.assert_called_with(bind=self.mock_engine,
                                          autocommit=False)
            self.assertEqual(checker.sess,
                             mock_maker.return_value.return_value)
            mock_sess = checker.sess
            # Test get project from database
            filter_by = checker.sess.query.return_value.filter_by
            filter_by.assert_called_with(id=1)
            self.assertEqual(checker.project,
                             filter_by.return_value.first.return_value)
        # Test context exit close session
        self.assertTrue(mock_sess.close.called)

    @mock.patch('scheduler.checker.ProjectChecker._sync_heads')
    @mock.patch('scheduler.checker.LOG')
    @mock.patch('os.makedirs')
    @mock.patch('shutil.rmtree')
    @mock.patch('git.Repo.clone_from')
    def test_create_archive(self, mock_git, mock_rmtree, mock_makedirs,
                            mock_log, _):
        """Test ProjectChecker._create_archive()"""
        # Mocking checker attributes
        checker = ProjectChecker(self.mock_engine, 1, "/")
        checker.project = mock.Mock(repository="repo")
        checker.sess = mock.Mock()
        checker.project.name = "test"
        # Normal call
        self.assertTrue(checker._create_archive())
        # Test make archive_dir
        mock_makedirs.assert_called_with("/test")
        # Test clone_from()
        mock_git.assert_called_with("repo", "/test")
        # Test update archive_dir in database
        self.assertEqual(checker.project.archive_path, "/test")
        self.assertTrue(checker.sess.commit.called)
        # Archive directory exist
        mock_makedirs.side_effect = OSError
        self.assertTrue(checker._create_archive())
        mock_rmtree.assert_called_with("/test")
        # Git error
        mock_git.side_effect = git.exc.GitCommandError("cmd", "error")
        self.assertFalse(checker._create_archive())
        # Test logging errors
        self.assertTrue(mock_log.error.called)

    @mock.patch('scheduler.checker.LOG')
    def test_sync_heads(self, mock_log):
        """Test ProjectChecker._sync_heads()"""
        checker = ProjectChecker(self.mock_engine, 1, "/")
        mock_repo = mock.Mock(heads=[])
        # Mocking list git.Head object's and pull()
        heads = [mock.Mock(), mock.Mock()]
        mock_pull = mock_repo.remotes.origin.pull
        # Mocking result repo.remotes.origin.pull()
        mock_pull.return_value = heads
        # Test create new remotely
        self.assertEqual(checker._sync_heads(mock_repo), True)
        # Test synchronize heads
        for info in heads:
            mock_repo.create_head.assert_any_call(
                info.ref.remote_head, info.ref)
        # Test delete old localy
        mock_repo.heads = [heads[0].ref.remote_head, heads[1].ref.remote_head]
        self.assertEqual(checker._sync_heads(mock_repo), True)
        # Test synchronize heads
        for head in mock_repo.heads:
            mock_repo.git.branch.assert_any_call('-D', head.name)
        # Git error
        mock_pull.side_effect = git.exc.GitCommandError("cmd", "error")
        self.assertEqual(checker._sync_heads(mock_repo), False)
        # Test logging errors
        self.assertTrue(mock_log.error.called)

    @mock.patch('%s.open' % ("builtins" if six.PY3 else "__builtin__"))
    @mock.patch('scheduler.checker.LOG')
    @mock.patch('os.path.exists', return_value=True)
    @mock.patch('yaml.load')
    def test_load_config(self, mock_load, mock_exists, mock_log, _):
        """Test ProjectChecker._load_config()"""
        # Mocking checker attributes
        checker = ProjectChecker(self.mock_engine, 1, "/")
        checker.project = mock.Mock(archive_path="repo")
        mock_load.return_value = {"test": "test"}
        # Normal call
        self.assertEqual(checker._load_config(), {"test": "test"})
        # fail load yaml
        mock_load.side_effect = yaml.YAMLError
        self.assertEqual(checker._load_config(), None)
        self.assertTrue(mock_log.error.called)
        # file not exists
        mock_exists.return_value = False
        self.assertEqual(checker._load_config(), None)
        self.assertTrue(mock_log.error.called)

    # sqlalchemy.orm.sessionmaker
    @mock.patch('scheduler.checker.ProjectChecker._sync_heads')
    @mock.patch('git.Repo')
    def test_check_update(self, mock_git, mock_sync):
        """Test ProjectChecker._check_update()"""
        # Mocking checker attributes
        checker = ProjectChecker(self.mock_engine, 1, "/")
        checker.project = mock.Mock(archive_path="repo")
        # Mocking git data
        head1 = mock.Mock()
        head1.name = "head1"
        head2 = mock.Mock()
        head2.name = "head2"
        mock_git.return_value.heads = [head1, head2]
        mock_git.return_value.iter_commits.return_value = [1, 2]
        mock_sync.return_value = True
        # Normal call
        self.assertEqual(checker._check_update(), {"head1": [1], "head2": [1]})
        mock_git.assert_called_with("repo")
        # New archive
        self.assertEqual(checker._check_update(new_archive=True),
                         {"head1": 2, "head2": 2})
        # Sync error
        mock_sync.return_value = False
        self.assertEqual(checker._check_update(), {})

    @mock.patch('rest.models.build.Build.__init__', return_value=None)
    @mock.patch('scheduler.checker.ProjectChecker._create_archive')
    @mock.patch('scheduler.checker.ProjectChecker._load_config')
    @mock.patch('scheduler.checker.ProjectChecker._check_update')
    @mock.patch('scheduler.checker.LOG')
    # pylint: disable=too-many-arguments
    def test_run(self, mock_log, mock_update,
                 mock_config, mock_archive, mock_build):
        """Test ProjectChecker.run()"""
        # Mocking checker attributes
        checker = ProjectChecker(self.mock_engine, 1, "/")
        checker.project = mock.Mock(archive_path="repo")
        checker.project.name = "test"
        checker.sess = mock.Mock()
        # Mock functions
        mock_config.return_value = {"test": "test"}
        # Mock data
        commit1 = mock.Mock(hexsha="b" * 40, message="msg")
        commit1.author.name = "foo"
        commits = {"master": [commit1, commit1]}
        mock_update.return_value = commits
        # Normal call
        checker.run()
        self.assertFalse(mock_archive.called)
        self.assertTrue(mock_config.called)
        # Test arguments pass to database
        self.assertEqual(len(mock_build.call_args_list), 2)
        for call in mock_build.call_args_list:
            self.assertEqual(call[0][2], {'test': 'test'})
            self.assertEqual(call[0][3], "b" * 40)
            self.assertEqual(call[0][4], "master")
            self.assertEqual(call[0][5], "foo")
            self.assertEqual(call[0][6], "msg")
        self.assertTrue(checker.sess.commit.called)
        # New archive
        checker.project.archive_path = ""
        checker.run()
        # New archive and bad create
        mock_archive.return_value = False
        checker.run()
        # Error load config
        checker.project.archive_path = "repo"
        mock_config.return_value = None
        checker.run()
        # None project
        checker.project = None
        checker.run()
        self.assertTrue(mock_log.error.called)
