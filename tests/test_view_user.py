"""
Tests for rest.views.user module
"""

import json
try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock
from tests import BaseCase
from rest.models.role import Role
from rest.models.team import Team
from rest.views.user import UserView


class TestUserView(BaseCase):

    """
    Test rest.views.user.UserView class
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Fill database
        # pylint: disable=no-member
        self.db_sess.add(Role('user'))
        self.db_sess.add(Role('admin'))
        self.db_sess.add(Team('test'))
        # Add views
        user_view = UserView.as_view('user_view')
        self.app.add_url_rule('/',
                              view_func=user_view,
                              methods=['GET', 'POST'])
        self.app.add_url_rule('/<int:user_id>',
                              view_func=user_view,
                              methods=['GET', 'PUT', 'DELETE'])
        self.test_data = json.dumps({"username": 'test',
                                     "password": 'test',
                                     "email": 'test@example.ru'})
        self.test_data2 = json.dumps({"username": 'test' + "2",
                                      "password": 'test',
                                      "email": 'test@example.ru' + "2"})

    @mock.patch('rest.views.user.UserView._access_control', return_value=None)
    def test_post(self, mock_ac):
        """Test UserView.post() method"""
        # Create new user
        resp = self.client.post("/",
                                data=self.test_data,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 201)
        # Create duplicate
        resp = self.client.post("/",
                                data=self.test_data,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        self.assertIn("user already exist", resp.data.decode('utf8'))
        # Create without data
        resp = self.client.post("/",
                                data="",
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Create without required parameters
        resp = self.client.post("/",
                                data=json.dumps({"email": "mail"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Create with id
        resp = self.client.post("/1",
                                data="",
                                content_type='application/json')
        self.assertEqual(resp.status_code, 405)
        # Access control call
        self.assertTrue(mock_ac.called)

    @mock.patch('rest.views.user.UserView._access_control', return_value=None)
    def test_get(self, mock_ac):
        """Test UserView.get() method"""
        # Create users
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        self.client.post("/",
                         data=self.test_data2,
                         content_type='application/json')
        # Get list of users
        resp = self.client.get("/",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(len(body['users']), 2)
        self.assertEqual(body['users'][0]['login'], 'test')
        self.assertEqual(body['users'][0]['email'], 'test@example.ru')
        self.assertEqual(body['users'][0]['roles'], ["user"])
        # Get first user
        resp = self.client.get("/1",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(len(body['users']), 1)
        self.assertEqual(body['users'][0]['login'], 'test')
        self.assertEqual(body['users'][0]['email'], 'test@example.ru')
        self.assertEqual(body['users'][0]['roles'], ["user"])
        # Get not found user
        resp = self.client.get("/1000",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)
        # Access control call
        self.assertTrue(mock_ac.called)

    @mock.patch('rest.views.user.UserView._access_control', return_value=None)
    def test_put(self, mock_ac):
        """Test UserView.put() method"""
        # Create users
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        self.client.post("/",
                         data=self.test_data2,
                         content_type='application/json')
        # Update password
        resp = self.client.put("/1",
                               data=json.dumps({"password": "new_pass"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Update email
        resp = self.client.put("/1",
                               data=json.dumps({"email": "newmail@test.com"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Update team
        resp = self.client.put("/1",
                               data=json.dumps({"team": "test"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Check new data
        resp = self.client.get("/1",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(body['users'][0]['email'], "newmail@test.com")
        self.assertEqual(body['users'][0]['teams'], ["test"])
        # Update not exist team
        resp = self.client.put("/1",
                               data=json.dumps({"team": "badteam"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)
        # Update duplicate
        resp = self.client.put("/2",
                               data=json.dumps({"email": "newmail@test.com"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Update non-existent user
        resp = self.client.put("/1000",
                               data="",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)
        # Access control call
        self.assertTrue(mock_ac.called)

    @mock.patch('rest.views.user.UserView._access_control', return_value=None)
    def test_delete(self, mock_ac):
        """Test UserView.delete() method"""
        # Create users
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        # Delete user
        resp = self.client.delete("/1",
                                  content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Delete non-existent user
        resp = self.client.delete("/1",
                                  content_type='application/json')
        self.assertEqual(resp.status_code, 404)
        # Access control call
        self.assertTrue(mock_ac.called)
