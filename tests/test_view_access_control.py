"""
Tests all _access_cotrol() functions from modules rest.views.*
"""

try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock
import flask
from flask_sqlalchemy_session import current_session
from tests import BaseCase
import rest.models as models
# views
from rest.views.build import BuildView
from rest.views.user import UserView


# pylint: disable=protected-access
class TestAccessControls(BaseCase):

    """
    Test rest.views.user.UserView class
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Fill database
        # pylint: disable=no-member
        # Roles
        role1 = models.role.Role('user')
        role2 = models.role.Role('admin')
        # Teams
        team1 = models.team.Team('team1')
        team2 = models.team.Team('team2')
        # Users
        user1 = models.user.User('fiona', 'password', 'fiona@ex.com')
        user2 = models.user.User('scarlett', 'password', 'scarl@ex.com')
        user3 = models.user.User('admin', 'admin', 'admin@pyci.com')
        # Projects
        project1 = models.project.Project(
            'project1', 'repo_url1', team1)
        project2 = models.project.Project(
            'project2', 'repo_url2', team2)
        self.db_sess.add(project1)
        self.db_sess.add(project2)
        # Runners
        runner1 = models.runner.Runner('localhost', '127.0.0.1', 'docker')
        runner1.project = project1
        runner2 = models.runner.Runner(
            'localhost2', '127.0.0.1', 'docker')
        runner2.project = project2
        self.db_sess.add(runner1)
        self.db_sess.add(runner2)
        # Builds
        self.db_sess.add(models.build.Build("build1", project1, "", ""))
        self.db_sess.add(models.build.Build("build2", project2, "", ""))
        # Relationships
        self.db_sess.add(models.role.Roles(role=role1, user=user1))
        self.db_sess.add(models.role.Roles(role=role1, user=user2))
        self.db_sess.add(models.role.Roles(role=role2, user=user3))
        self.db_sess.add(models.team.Teams(team=team1, user=user1))
        self.db_sess.add(models.team.Teams(team=team1, user=user2))
        self.db_sess.add(models.team.Teams(team=team2, user=user2))
        # Commit
        self.db_sess.commit()

    @mock.patch('rest.views.user.abort')  # mock flask.abort()
    def test_ac_user(self, mock_abort):
        """Test UserView._access_control() method"""
        with self.app.app_context():
            flask.g.claims = {"scp": ["admin"]}
            self.assertEqual(UserView()._access_control(), None)
            flask.g.claims = {"scp": ["user"], "cid": 1}
            self.assertEqual(UserView()._access_control(1), None)
            flask.g.claims = {"scp": ["user"], "cid": 1}
            UserView()._access_control(2)
            mock_abort.assert_called_with(403)

    @mock.patch('rest.views.build.abort')  # mock flask.abort()
    def test_ac_build(self, mock_abort):
        """Test BuildView._access_control() method"""
        with self.app.app_context():
            projects = current_session.query(models.project.Project).all()
            # Admin
            flask.g.claims = {"scp": ["admin"]}
            self.assertEqual(BuildView()._access_control(projects[0]), None)
            # User from team
            flask.g.claims = {"scp": ["user"], "cid": 1}
            self.assertEqual(BuildView()._access_control(projects[0]), None)
            # User not from team
            flask.g.claims = {"scp": ["user"], "cid": 1}
            BuildView()._access_control(projects[1])
            mock_abort.assert_called_with(403)
            with self.app.test_request_context('/'):
                # Runner from project
                flask.g.claims = {"scp": ["runner"], "cid": 1}
                self.assertEqual(BuildView()._access_control(projects[0]),
                                 None)
                # Runner not from project
                flask.g.claims = {"scp": ["runner"], "cid": 1}
                BuildView()._access_control(projects[1])
                mock_abort.assert_called_with(403)
            # Test not allow method for runner
            with self.app.test_request_context('/', method='DELETE'):
                # Runner from project
                flask.g.claims = {"scp": ["runner"], "cid": 1}
                BuildView()._access_control(projects[0])
                mock_abort.assert_called_with(403)
