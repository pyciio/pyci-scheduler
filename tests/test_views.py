"""
Tests for rest.views.__init__
"""

from tests import BaseCase
import rest.views


class TestViewsInit(BaseCase):
    """
    Test rest.views.register_handlers()
    """

    def test_register_handlers(self):
        """Test views registration"""
        rest.views.register_handlers(self.app)
        self.assertIn('user_view', self.app.view_functions)
        self.assertIn('auth_view', self.app.view_functions)
        self.assertIn('team_view', self.app.view_functions)
        self.assertIn('role_view', self.app.view_functions)
        self.assertIn('project_view', self.app.view_functions)
        # Testing method from:
        # https://github.com/pallets/werkzeug/blob/master/tests/test_routing.py
        rule_map = self.app.url_map
        adapter = rule_map.bind('example.org', '/')
        self.assertEqual(adapter.match('/users'), ('user_view', {}))
        with self.assertRaises(Exception) as _:  # raise without JSON request
            self.assertEqual(adapter.match('/authenticate'), ('auth_view', {}))
        self.assertEqual(adapter.match('/teams'), ('team_view', {}))
        self.assertEqual(adapter.match('/roles'), ('role_view', {}))
        self.assertEqual(adapter.match('/projects'), ('project_view', {}))
