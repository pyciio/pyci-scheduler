"""
Tests for module scheduler.config
"""

try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock
import unittest
from scheduler.config import Config


class TestSchedulerConfig(unittest.TestCase):
    """Testing scheduler.config.Config class"""

    def test_init(self):
        """Test Config.__init__()"""
        self.assertEqual(Config({"test": "test"}), {"test": "test"})

    # werkzeug.utils.import_string
    @mock.patch('scheduler.config.import_string')
    def test_from_object(self, mock_import):
        """Test Config.from_object()"""
        config = Config()
        # Module as string
        config.from_object("module")
        mock_import.assert_called_with("module")
        # Module as object
        config = Config()
        config.from_object(mock.MagicMock(TEST="test"))
        self.assertEqual(config, {"TEST": "test"})
        config = Config()
        config.from_object(mock.MagicMock(test="test"))
        self.assertEqual(config, {})

    def test_repr(self):
        """Test Config.__repr__()"""
        self.assertEqual(repr(Config()), "<Config {}>")
