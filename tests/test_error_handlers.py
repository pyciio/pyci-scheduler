"""
Tests for rest.errors module
"""

from flask import abort
import flask.views
from tests import BaseCase
from rest import errors


class TestRestErrors(BaseCase):

    """
    Test rest.errors.register_handlers()
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # error handlers
        errors.register_handlers(self.app)
        # Testing routes
        # pylint: disable=missing-docstring

        class HTTPExcpt(flask.views.MethodView):

            def get(self):  # pylint: disable=no-self-use
                abort(500)
        self.app.add_url_rule('/httpexcpt',
                              view_func=HTTPExcpt.as_view('httpexcpt'))
        # pylint: disable=missing-docstring

        class Excpt(flask.views.MethodView):

            def get(self):  # pylint: disable=no-self-use
                raise Exception()
        self.app.add_url_rule('/excpt',
                              view_func=Excpt.as_view('excpt'))

    def test_handlers(self):
        """
        Test error handlers Exception and HTTPException
        """
        resp = self.client.get("/httpexcpt")
        self.assertEqual(resp.status_code, 500)
        resp = self.client.get("/excpt")
        self.assertEqual(resp.status_code, 500)
