"""
Tests for rest.views.auth module
"""

import json

from tests import BaseCase
from rest.models.user import User
from rest.views.auth import AuthView
from rest.middleware import auth


class TestAuthView(BaseCase):
    """
    Test rest.views.auth.AuthView class
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Fill databases
        # pylint: disable=no-member
        self.db_sess.add(User('test', 'test', 'test@example.ru'))
        self.db_sess.commit()
        # Add views
        jwt_auth = auth.JWTAuth(self.app.config['JWT_RSA_PRIV'],
                                self.app.config['JWT_RSA_PUB'])
        self.app.add_url_rule('/',
                              view_func=AuthView.as_view('auth_view',
                                                         jwt_auth=jwt_auth))

    def _login(self, user=None, password=None):
        if user is None or password is None:
            json_auth = ""
        else:
            json_auth = json.dumps({"username": user, "password": password})
        return self.client.post("/", data=json_auth,
                                content_type='application/json')

    def test_new_user(self):
        """Test authenticate view"""
        # Empty JSON
        resp = self._login()
        self.assertEqual(resp.status_code, 401)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertIn('error', body)
        self.assertEqual(body['error'], "unauthorized")
        # Good user
        resp = self._login('test', 'test')
        self.assertEqual(resp.status_code, 200)
        self.assertIn('token', json.loads(resp.data.decode('utf-8')))
        # Bad user
        resp = self._login("baduser", "baduser")
        self.assertEqual(resp.status_code, 401)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertIn('error', body)
        self.assertEqual(body['error'], "invalid credentials")
