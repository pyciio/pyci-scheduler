"""
Tests for rest.views.project module
"""

import json
from tests import BaseCase
from rest.models.team import Team
from rest.views.project import ProjectView


class TestProjectView(BaseCase):
    """
    Test rest.views.project.ProjectView class
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Fill database
        # pylint: disable=no-member
        self.db_sess.add(Team('test'))
        self.db_sess.add(Team('test2'))
        # Add views
        project_view = ProjectView.as_view('project_view')
        self.app.add_url_rule('/',
                              view_func=project_view,
                              methods=['GET', 'POST'])
        self.app.add_url_rule('/<int:project_id>',
                              view_func=project_view,
                              methods=['GET', 'PUT', 'DELETE'])
        self.test_data = json.dumps({"name": 'test',
                                     "repository": 'repo_url',
                                     "team": 'test'})
        self.test_data2 = json.dumps({"name": 'test' + "2",
                                      "repository": 'repo_url' + "2",
                                      "team": 'test'})

    def test_post(self):
        """Test ProjectView.post() method"""
        # Create new project
        resp = self.client.post("/",
                                data=self.test_data,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 201)
        # Create duplicate
        resp = self.client.post("/",
                                data=self.test_data,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        self.assertIn("project already exist", resp.data.decode('utf8'))
        # Create without data
        resp = self.client.post("/",
                                data="",
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Create without required parameters
        resp = self.client.post("/",
                                data=json.dumps({"repository": "test",
                                                 "team": "test"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        resp = self.client.post("/",
                                data=json.dumps({"name": "test",
                                                 "team": "test"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        resp = self.client.post("/",
                                data=json.dumps({"name": "test",
                                                 "repository": "test"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Create with not exist team
        resp = self.client.post("/",
                                data=json.dumps({"name": "test",
                                                 "repository": "test",
                                                 "team": "badteam"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, 404)
        # Create with id
        resp = self.client.post("/1",
                                data="",
                                content_type='application/json')
        self.assertEqual(resp.status_code, 405)

    def test_get(self):
        """Test ProjectView.get() method"""
        # Create projects
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        self.client.post("/",
                         data=self.test_data2,
                         content_type='application/json')
        # Get list of projects
        resp = self.client.get("/",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(len(body['projects']), 2)
        self.assertEqual(body['projects'][0]['name'], 'test')
        self.assertEqual(body['projects'][0]['repository'], 'repo_url')
        # Get first project
        resp = self.client.get("/1",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(len(body['projects']), 1)
        self.assertEqual(body['projects'][0]['name'], 'test')
        self.assertEqual(body['projects'][0]['repository'], 'repo_url')
        # Get not exist project
        resp = self.client.get("/1000",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)

    def test_put(self):
        """Test ProjectView.put() method"""
        # Create projects
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        self.client.post("/",
                         data=self.test_data2,
                         content_type='application/json')
        # Update name
        resp = self.client.put("/1",
                               data=json.dumps({"name": "new_name"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Update repository
        resp = self.client.put("/1",
                               data=json.dumps({"repository": "new_repo"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Update team
        resp = self.client.put("/1",
                               data=json.dumps({"team": "test2"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Update with not exist team
        resp = self.client.put("/1",
                               data=json.dumps({"team": "badteam"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)
        # Check new data
        resp = self.client.get("/1",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(body['projects'][0]['name'], "new_name")
        self.assertEqual(body['projects'][0]['repository'], "new_repo")
        self.assertEqual(body['projects'][0]['team'], "test2")
        # Update duplicate
        resp = self.client.put("/2",
                               data=json.dumps({"name": "new_name"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Update non-existent project
        resp = self.client.put("/1000",
                               data="",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)

    def test_delete(self):
        """Test ProjectView.delete() method"""
        # Create project
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        # Delete project
        resp = self.client.delete("/1",
                                  content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Delete non-existent project
        resp = self.client.delete("/1",
                                  content_type='application/json')
        self.assertEqual(resp.status_code, 404)
