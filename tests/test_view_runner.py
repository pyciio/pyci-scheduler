"""
Tests for rest.views.runner module
"""

import json
from tests import BaseCase
from rest.models.team import Team
from rest.models.project import Project
from rest.models.runner import Runner
from rest.middleware import auth
from rest.views.runner import RunnerView, RegisterRunnerView


class TestRunnerView(BaseCase):
    """
    Test rest.views.runner.RunnerView class
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Add views
        runner_view = RunnerView.as_view('runner_view')
        self.app.add_url_rule('/',
                              view_func=runner_view,
                              methods=['GET'])
        self.app.add_url_rule('/<int:runner_id>',
                              view_func=runner_view,
                              methods=['GET', 'PUT', 'DELETE'])

    def _fill_database(self):
        """Fill database testing data"""
        # pylint: disable=no-member
        project = Project('project1', 'test', Team('test'))
        runner1 = Runner('test', '127.0.0.1', 'docker')
        runner1.token = ""
        runner1.project = project
        self.db_sess.add(runner1)
        runner2 = Runner('test2', '127.0.0.1', 'docker')
        runner2.token = ""
        runner2.project = project
        self.db_sess.add(runner2)

    def test_get(self):
        """Test RunnerView.get() method"""
        self._fill_database()
        # Get list of runners
        resp = self.client.get("/",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(len(body['runners']), 2)
        self.assertEqual(body['runners'][0]['name'], 'test')
        self._fill_database()
        # Get first runner
        resp = self.client.get("/1",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(len(body['runners']), 1)
        self.assertEqual(body['runners'][0]['name'], 'test')
        # Get not exist runner
        resp = self.client.get("/1000",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)

    def test_put(self):
        """Test RunnerView.put() method"""
        # Update name
        self._fill_database()
        resp = self.client.put("/1",
                               data=json.dumps({"name": "new_name"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Update executor
        resp = self.client.put("/1",
                               data=json.dumps({"executor": "shell"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Update ip
        resp = self.client.put("/1",
                               data=json.dumps({"ip": "127.0.0.2"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Bad type executor
        resp = self.client.put("/1",
                               data=json.dumps({"executor": "bad"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Check new data
        resp = self.client.get("/1",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(body['runners'][0]['name'], "new_name")
        self.assertEqual(body['runners'][0]['executor'], "shell")
        self.assertEqual(body['runners'][0]['ip'], "127.0.0.2")
        # Update non-existent runners
        resp = self.client.put("/1000",
                               data="",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)

    def test_delete(self):
        """Test RunnerView.delete() method"""
        self._fill_database()
        # Delete runner
        resp = self.client.delete("/1",
                                  content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Delete non-existent runner
        resp = self.client.delete("/1",
                                  content_type='application/json')
        self.assertEqual(resp.status_code, 404)


class TesRegisterRunnerView(BaseCase):
    """
    Test rest.views.runner.RegisterRunnerView class
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Fill database
        # pylint: disable=no-member
        project = Project('project1', 'test', Team('test'))
        project.token = "test_token"
        self.db_sess.add(project)
        # Add views
        jwt_auth = auth.JWTAuth(self.app.config['JWT_RSA_PRIV'],
                                self.app.config['JWT_RSA_PUB'])
        # Add views
        reg_view = RegisterRunnerView.as_view('reg_view', jwt_auth=jwt_auth)
        self.app.add_url_rule('/',
                              view_func=reg_view,
                              methods=['POST'])
        self.test_data = json.dumps({"name": 'test',
                                     "project": 'project1',
                                     "ip": "127.0.0.1",
                                     "executor": "docker",
                                     "token": "test_token"})
        self.test_data2 = json.dumps({"name": 'test2',
                                      "project": 'project2',
                                      "ip": "127.0.0.1",
                                      "executor": "docker",
                                      "token": "test_token"})

    def _check_required(self):
        required = ["name", "project", "ip", "executor", "token"]
        for del_param in required:
            conf = {}
            for param in required:
                if param != del_param:
                    conf[param] = ""
            resp = self.client.post("/",
                                    data=json.dumps(conf),
                                    content_type='application/json')
            self.assertEqual(resp.status_code, 400)

    def test_post(self):
        """Test RegisterRunnerView.post() method"""
        # Register new runner
        resp = self.client.post("/",
                                data=self.test_data,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 201)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertIn("token", body)
        # Create duplicate
        resp = self.client.post("/",
                                data=self.test_data,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        self.assertIn("runner already register", resp.data.decode('utf8'))
        # Create without data
        resp = self.client.post("/",
                                data="",
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Create without data
        resp = self.client.post("/",
                                data=self.test_data.replace(
                                    "test_token", "bad"),
                                content_type='application/json')
        self.assertEqual(resp.status_code, 401)
        # Create without required parameters
        self._check_required()
        # Create with not exist project
        resp = self.client.post("/",
                                data=self.test_data2,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 404)
