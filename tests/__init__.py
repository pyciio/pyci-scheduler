"""
Unittest module for application.
"""

import json
import unittest
import flask
from flask_sqlalchemy_session import flask_scoped_session
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
# Database models
from rest.models import Base


class BaseCase(unittest.TestCase):

    """
    Base test case class. Create flask application object fro testing,
    create database connection and database session.

    All other tests must inherit an application and a database session from
    this class.

    Application use configuration config.TestingConfig.
    """

    def setUp(self):
        self.app = flask.Flask("test")
        self.app.config.from_object('config.TestingConfig')
        self.client = self.app.test_client(self)
        # Create database
        db_uri = self.app.config.get('SQLALCHEMY_DATABASE_URI')
        db_debug = self.app.config.get('SQLALCHEMY_ECHO')
        self.db_engine = create_engine(db_uri, echo=db_debug)
        Base.metadata.create_all(self.db_engine)
        # Session
        session_factory = sessionmaker(bind=self.db_engine, autocommit=False)
        self.db_sess = flask_scoped_session(session_factory, self.app)

    def tearDown(self):
        self.db_sess.remove()
        Base.metadata.drop_all(self.db_engine)

    def api(self, method, url="/", data=None, code=200):
        """
        Request API function.

        Call URL 'url' with method 'method' and data 'data'. After receiving a
        response check status_code and try convert response body to dict.

        Return body: dict or string or None
        """
        if data is not None:
            data = json.dumps(data)
        resp = self.client.open(url, method=method, data=data,
                                content_type='application/json',
                                as_tuple=False)
        # pylint: disable=no-member
        self.assertNotEqual(resp, None)
        self.assertEqual(resp.status_code, code)
        if resp.data is not None:
            try:
                return json.loads(resp.data.decode('utf-8'))
            # In python 2.7 there is no exception JSONDecodeError
            except ValueError:
                return resp.data.decode('utf-8')
