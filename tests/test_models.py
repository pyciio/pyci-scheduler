"""
Test for rest.models.* modules
"""

import rest.models as models
from rest.models.password import Password, PasswordHash
from rest.models.ipaddr import IPAddress
from tests import BaseCase


# pylint: disable=protected-access
class TestModels(BaseCase):
    """
    Test all models from rest.models
    """

    def test_password(self):
        """
        Test PasswordHash class
        """
        # Repr and bad hash type
        _hash = PasswordHash.new("test", 10)
        self.assertEqual(repr(_hash), '<PasswordHash>')
        self.assertNotEqual(_hash, 0)
        # Bad type for convert
        _password = Password()

        self.assertRaises(TypeError, _password._convert, 1)

    def test_repr(self):
        """
        Test __repr__() method for all models
        """
        # User model
        self.assertEqual(repr(models.user.User(None, None, None)),
                         "<User 'None'>")
        # Role model
        self.assertEqual(repr(models.role.Role('test')), "<Role 'test'>")
        self.assertEqual(models.role.Role('test', 'test').description, 'test')
        # Roles model
        self.assertEqual(repr(models.role.Roles()), "<Roles 'None'>")
        # Team model
        self.assertEqual(repr(models.team.Team('test')), "<Team 'test'>")
        self.assertEqual(models.team.Team('test', 'test').description, 'test')
        # Teams model
        self.assertEqual(repr(models.team.Teams()), "<Teams 'None'>")
        # Project model
        self.assertEqual(repr(models.project.Project('test', 'test', None)),
                         "<Project 'test'>")
        # Build model
        self.assertEqual(repr(models.build.Build(
            'test', None, None, "")), "<Build 'test'>")
        # Runner model
        self.assertEqual(repr(models.runner.Runner(
            'test', None, None)), "<Runner 'test'>")

    def test_build(self):
        """Test rest.models.build.Build class"""
        # Build model
        build = models.build.Build('test', None, None, "")
        # Check 'run'
        build.update_status("run")
        self.assertNotEqual(build.started, None)
        self.assertEqual(build.status, "run")
        # Check status ended
        for status in ["successful", "failed", "stoped"]:
            build.update_status(status)
            self.assertNotEqual(build.ended, None)
            self.assertEqual(status, build.status)
            build.ended = None

    def test_ipaddress(self):
        """Test rest.models.ipaddr.IPAddress class"""
        # Test _ip_to_int()
        try:
            IPAddress()._ip_to_int("")
        except TypeError as err:
            self.assertIn("Cannot convert", str(err))
        try:
            IPAddress()._ip_to_int("500.500.500.500")
        except TypeError as err:
            self.assertIn("Cannot convert", str(err))
        # Test _int_to_ip()
        try:
            IPAddress()._int_to_ip(-1)
        except TypeError as err:
            self.assertIn("Cannot convert", str(err))
        try:
            IPAddress()._int_to_ip(4294967296)
        except TypeError as err:
            self.assertIn("Cannot convert", str(err))
        # Test _convert()
        try:
            IPAddress()._convert(-1)
        except TypeError as err:
            self.assertIn("Out of range", str(err))
        try:
            IPAddress()._convert(4294967296)
        except TypeError as err:
            self.assertIn("Out of range", str(err))
        try:
            IPAddress()._convert({})
        except TypeError as err:
            self.assertIn("Cannot convert", str(err))
        self.assertEqual(IPAddress()._convert(50), 50)
