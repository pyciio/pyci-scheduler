"""
Tests for rest.__init__
"""

try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock
from tests import BaseCase
import rest


class TestRestInit(BaseCase):
    """Test rest.__init__"""

    @mock.patch('rest.errors.register_handlers')
    @mock.patch('rest.before.register_handlers')
    @mock.patch('rest.views.register_handlers')
    # It's flask_sqlalchemy_session.flask_scoped_sessions
    @mock.patch('rest.flask_scoped_session')
    @mock.patch('rest.create_engine')  # It's sqlalchemy.create_engine
    @mock.patch('rest.sessionmaker')  # It's sqlalchemy.orm.sessionmaker
    @mock.patch('os.getenv')
    # pylint: disable=too-many-arguments
    def test_create_app(self, mock_env, mock_maker, mock_engine, mock_scoped,
                        mock_views, mock_before, mock_errors):
        """Test rest.__init__.create_app()"""
        app = rest.create_app()
        # Check setup database
        self.assertTrue(mock_engine.called)
        self.assertTrue(mock_maker.called)
        mock_scoped.assert_called_with(mock_maker.return_value, app)
        # Check setup handlers and views
        mock_views.assert_called_with(app)
        mock_before.assert_called_with(app)
        mock_errors.assert_called_with(app)
        # Test production/develop config enviroment
        mock_env.assert_called_with('PYCI_DEV', False)
        mock_env.return_value = False
        self.assertFalse(rest.create_app().debug)
        mock_env.return_value = True
        self.assertTrue(rest.create_app().debug)
