"""
Tests for rest.middleware.auth
"""

import jwt
from rest.middleware import auth
from tests import BaseCase


class TestMdlwrAuth(BaseCase):

    """Test rest.middleware.auth.JWTAuth and
    rest.middleware.auth.JWTAuthError"""

    def test_check_token(self):
        """
        Test JWTAuth.check_token()
        """
        jwt_auth = auth.JWTAuth(self.app.config['JWT_RSA_PRIV'],
                                self.app.config['JWT_RSA_PUB'], -1)
        expired_token = jwt_auth.get_user_token(
            1, ["user", "admin"])  # expired token
        # Token is invalid
        excpt = None
        try:
            jwt_auth.check_token("badtoken")
        except auth.JWTAuthError as err:
            excpt = err
        self.assertEqual(str(excpt), "Token is invalid")
        # Token has expired
        excpt = None
        try:
            jwt_auth.check_token(expired_token)
        except auth.JWTAuthError as err:
            excpt = err
        self.assertEqual(str(excpt), "Token has expired")
        # Token bad format
        excpt = None
        bad_claims_token = jwt.encode(payload={"iat": 1},
                                      key="key_string",
                                      algorithm='HS256').decode('utf-8')
        try:
            jwt_auth.check_token(bad_claims_token)
        except auth.JWTAuthError as err:
            excpt = err
        self.assertEqual(str(excpt), "Unauthorized")

    def test_get_user_token(self):
        """
        Test JWTAuth.get_user_token()
        """
        jwt_auth = auth.JWTAuth(self.app.config['JWT_RSA_PRIV'],
                                self.app.config['JWT_RSA_PUB'],
                                self.app.config['JWT_EXPIRE'])
        token = jwt_auth.get_user_token(1, ["user", "admin"])
        claims = jwt_auth.check_token(token)
        self.assertEqual(claims['cid'], 1)
        self.assertEqual(len(claims['scp']), 2)
        self.assertIn('user', claims['scp'])
        self.assertIn('admin', claims['scp'])
