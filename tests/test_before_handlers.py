"""
Test for rest.before module
"""

import json
import flask
import flask.views
from tests import BaseCase
from rest import before
from rest.middleware import auth


class TestRestBefore(BaseCase):

    """
    Test rest.before.register_handlers()
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Before views
        # self.app.before_request_funcs.setdefault(
        #     None, []).append(before.check_jwt)
        before.register_handlers(self.app)
        # Check view
        # pylint: disable=missing-docstring

        class Index(flask.views.MethodView):

            def get(self):  # pylint: disable=no-self-use
                return "index"
        self.app.add_url_rule('/',
                              view_func=Index.as_view('index'))
        # Token for tests
        jwt_auth = auth.JWTAuth(self.app.config['JWT_RSA_PRIV'],
                                self.app.config['JWT_RSA_PUB'])
        self.token = jwt_auth.get_user_token(
            1, ["user", "admin"])  # expired token

    def _get_test(self, headers=None):
        return self.client.get("/",
                               headers=headers,
                               content_type='application/json')

    def test_check_jwt(self):
        """
        Test authorization handler.
        """
        # Without auth header
        resp = self._get_test()
        self.assertEqual(resp.status_code, 401)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertIn('error', body)
        self.assertIn("Bad header.", body['error'])
        # Bad auth token header type
        resp = self._get_test(
            {"Authorization": "Badtype {0}".format(self.token)})
        self.assertEqual(resp.status_code, 401)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertIn('error', body)
        self.assertIn("Bad header.", body['error'])
        # Bad auth token
        resp = self._get_test({"Authorization": "Bearer badtoken"})
        self.assertEqual(resp.status_code, 401)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertIn('error', body)
        self.assertEqual(body['error'], "Token is invalid")
        # Good token
        resp = self._get_test(
            {"Authorization": "Bearer {0}".format(self.token)})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data.decode('utf-8'), "index")
        # /authenticate URL
        resp = self.client.get("/authenticate",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)  # 404 - It's good code
        # /register URL
        resp = self.client.get("/register",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)  # 404 - It's good code
