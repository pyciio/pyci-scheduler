"""
Tests for module scheduler
"""

import os
try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock
import unittest
import logging
from scheduler import Scheduler

# pylint: disable=protected-access


class TestSchduler(unittest.TestCase):
    """Testing scheduler.Scheduler class"""

    @mock.patch('os.getenv')
    # sqlalchemy.orm.sessionmaker
    @mock.patch('scheduler.sessionmaker')
    # sqlalchemy.create_engine
    @mock.patch('scheduler.create_engine')
    def test_init(self, mock_engine, mock_maker, mock_env):
        """Test Scheduler.__init__()"""
        mock_env.return_value = False
        scheduler = Scheduler()
        mock_env.assert_called_with('PYCI_DEV', False)
        self.assertTrue(mock_engine.called)
        self.assertTrue(mock_maker.called)
        self.assertEqual(scheduler.sess_maker, mock_maker.return_value)
        # Test debug enviroment
        mock_env.return_value = True
        scheduler = Scheduler()

    @mock.patch('scheduler.Scheduler.__init__', return_value=None)
    @mock.patch('scheduler.checker.ProjectChecker.__exit__')
    @mock.patch('scheduler.checker.ProjectChecker.__init__', return_value=None)
    @mock.patch('scheduler.checker.ProjectChecker.__enter__')
    # pylint: disable=unused-argument
    def test_checker(self, mock_enter, *args):
        """Test Scheduler.__checker()"""
        scheduler = Scheduler()
        scheduler.db_engine = None
        scheduler.config = {'REPO_ARCHIVE': ''}
        scheduler._checker(1)
        self.assertTrue(mock_enter.called)
        self.assertTrue(mock_enter.return_value.run.called)

    @mock.patch('scheduler.Scheduler.__init__', return_value=None)
    @mock.patch('time.sleep', side_effect=KeyboardInterrupt)
    # pylint: disable=unused-argument
    def test_run(self, *args):
        """Test Scheduler.run()"""
        scheduler = Scheduler()
        # mocking sessonmaker
        scheduler.sess_maker = mock.Mock()
        scheduler.min_check_interval = 5
        scheduler._checker = mock.Mock()
        # pylint: disable=no-member
        proj_query = scheduler.sess_maker.return_value.query
        # Mocking query(Project).all()
        projects = [mock.Mock(), mock.Mock()]
        proj_query.return_value.all.return_value = projects
        self.assertEqual(scheduler.run(), None)

    @mock.patch('scheduler.Scheduler.__init__', return_value=None)
    @mock.patch('daemonize.Daemonize.start')
    @mock.patch('scheduler.LOG')
    @mock.patch('daemonize.Daemonize.__init__', return_value=None)
    # pylint: disable=unused-argument
    def test_run_as_daemon(self, mock_init, mock_log, mock_start, *args):
        """Test Scheduler.run_as_daemon()"""
        hdlr = logging.FileHandler("./tests/test_scheduler.log")
        mock_log.handlers = [hdlr]
        scheduler = Scheduler()
        scheduler.config = {'PID_FILE': '', 'PROJECT_ROOT': ''}
        scheduler.run_as_daemon("u", "g", "p")
        mock_init.assert_called_with(action=scheduler.run,
                                     app='pyci-scheduler',
                                     chdir='', keep_fds=[hdlr.stream.fileno()],
                                     pid="p",
                                     user="u",
                                     group="g",
                                     logger=mock_log)
        self.assertTrue(mock_start.called)
        hdlr.close()
        os.remove("./tests/test_scheduler.log")
