"""
Tests for rest.views.build module
"""

try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock
import flask
from tests import BaseCase
from rest.models.team import Team
from rest.models.project import Project
from rest.models.build import Build
from rest.views.build import BuildView


class TestBuildView(BaseCase):

    """
    Test rest.views.build.BuildView class
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Fill database
        # pylint: disable=no-member
        project = Project('project1', 'test', Team('test'))
        for indx in range(2):
            self.db_sess.add(
                Build("test%s" % indx, project, "{\"pipeline\": []}", ""))
        self.db_sess.commit()
        # Add views
        build_view = BuildView.as_view('build_view')
        self.app.add_url_rule('/',
                              view_func=build_view,
                              methods=['GET', 'POST'])
        self.app.add_url_rule('/<int:build_id>',
                              view_func=build_view,
                              methods=['GET', 'PUT', 'DELETE'])

    @mock.patch('rest.views.build.BuildView._access_control')
    def test_post(self, mock_ac):
        """Test BuildView.post() method"""
        # Create normal
        body = self.api("POST", "/", {"name": "test2",
                                      "project": "project1",
                                      "config": "",
                                      "commit_hash": ""}, 201)
        # Create duplicate
        body = self.api("POST", "/", {"name": "test0",
                                      "project": "project1",
                                      "config": "",
                                      "commit_hash": ""}, 400)
        self.assertIn("build already exist", body)
        # Create without data
        self.api("POST", "/", {}, 400)
        # Create without required parameters
        required = ["name", "project", "config", "hash"]
        for del_param in required:
            conf = {}
            for param in required:
                if param != del_param:
                    conf[param] = ""
            self.api("POST", "/", conf, 400)
        # Create with not exist project
        body = self.api("POST", "/", {"name": "test0",
                                      "project": "bad_project",
                                      "config": "",
                                      "commit_hash": ""}, 404)
        # Create with id
        self.api("POST", "/1", {}, 405)
        # Check call access_control
        self.assertTrue(mock_ac.called)

    @mock.patch('rest.views.build.BuildView._access_control')
    def test_get(self, mock_ac):
        """Test BuildView.get() method"""
        # Get list of builds
        with self.app.app_context():
            flask.g.claims = {"scp": ["admin"]}
            body = self.api("GET", "/")
            self.assertEqual(len(body['builds']), 2)
            self.assertEqual(body['builds'][0]['name'], 'test0')
        # Get first build
        with self.app.app_context():
            flask.g.claims = {"scp": ["admin"]}
            body = self.api("GET", "/1")
            self.assertEqual(len(body['builds']), 1)
            self.assertEqual(body['builds'][0]['name'], 'test0')
        # Get not exist build
        with self.app.app_context():
            flask.g.claims = {"scp": ["admin"]}
            self.api("GET", "/1000", None, 404)
        # Check access control to all builds
        with self.app.app_context():
            flask.g.claims = {"scp": ["user"]}
            self.api("GET", "/", None, 403)
        # Check call access_control
        self.assertTrue(mock_ac.called)

    @mock.patch('rest.views.build.BuildView._access_control')
    def test_put(self, mock_ac):
        """Test BuildView.put() method"""
        # Update param logs
        body = self.api("PUT", "/1", {"logs": "new_logs"})
        # Check new data
        body = self.api("GET", "/1")
        self.assertEqual(body.get('builds')[0]["logs"], "new_logs")
        # Update status
        for status in ["successful", "failed", "stoped", "run"]:
            body = self.api("PUT", "/1", {"status": status})
            body = self.api("GET", "/1")
            self.assertEqual(body.get('builds')[0]["status"], status)
        # Bad status
        self.api("PUT", "/1", {"status": "bad"}, 400)
        # Update non-existent build
        self.api("PUT", "/1000", "", 404)
        # Check call access_control
        self.assertTrue(mock_ac.called)

    @mock.patch('rest.views.build.BuildView._access_control')
    def test_delete(self, mock_ac):
        """Test BuildView.delete() method"""
        # Delete build
        self.api("DELETE", "/1")
        # Delete non-existent build
        self.api("DELETE", "/1", None, 404)
        # Check call access_control
        self.assertTrue(mock_ac.called)
