"""
Tests for rest.views.role module
"""

import json
from tests import BaseCase
from rest.models.user import User
from rest.views.role import RoleView


class TestRoleView(BaseCase):
    """
    Test rest.views.role.RoleView class
    """

    def setUp(self):
        super(self.__class__, self).setUp()
        # Fill database
        # pylint: disable=no-member
        self.db_sess.add(User('test', 'test', 'test@example.com'))
        # Add views
        role_view = RoleView.as_view('role_view')
        self.app.add_url_rule('/',
                              view_func=role_view,
                              methods=['GET', 'POST'])
        self.app.add_url_rule('/<int:role_id>',
                              view_func=role_view,
                              methods=['GET', 'PUT', 'DELETE'])
        self.test_data = json.dumps({"name": 'test',
                                     "description": 'blabla'})
        self.test_data2 = json.dumps({"name": 'test' + "2",
                                      "description": 'blabla' + "2"})

    def test_post(self):
        """Test RoleView.post() method"""
        # Create new role
        resp = self.client.post("/",
                                data=self.test_data,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 201)
        # Create duplicate
        resp = self.client.post("/",
                                data=self.test_data,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        self.assertIn("role already exist", resp.data.decode('utf8'))
        # Create without data
        resp = self.client.post("/",
                                data="",
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Create without required parameters (no name)
        resp = self.client.post("/",
                                data=json.dumps({"description": "test"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Create with id
        resp = self.client.post("/1",
                                data="",
                                content_type='application/json')
        self.assertEqual(resp.status_code, 405)

    def test_get(self):
        """Test RoleView.get() method"""
        # Create roles
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        self.client.post("/",
                         data=self.test_data2,
                         content_type='application/json')
        # Get list of roles
        resp = self.client.get("/",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(len(body['roles']), 2)
        self.assertEqual(body['roles'][0]['name'], 'test')
        self.assertEqual(body['roles'][0]['description'], 'blabla')
        # Get first role
        resp = self.client.get("/1",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(len(body['roles']), 1)
        self.assertEqual(body['roles'][0]['name'], 'test')
        self.assertEqual(body['roles'][0]['description'], 'blabla')
        # Get not found role
        resp = self.client.get("/1000",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)

    def test_put(self):
        """Test RoleView.put() method"""
        # Create roles
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        self.client.post("/",
                         data=self.test_data2,
                         content_type='application/json')
        # Update name
        resp = self.client.put("/1",
                               data=json.dumps({"name": "new_name"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Update description
        resp = self.client.put("/1",
                               data=json.dumps({"description": "new_descr"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Check new data
        resp = self.client.get("/1",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(body['roles'][0]['name'], "new_name")
        self.assertEqual(body['roles'][0]['description'], "new_descr")
        # Update duplicate
        resp = self.client.put("/2",
                               data=json.dumps({"name": "new_name"}),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        # Update non-existent role
        resp = self.client.put("/1000",
                               data="",
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)

    def test_delete(self):
        """Test RoleView.delete() method"""
        # Create role
        self.client.post("/",
                         data=self.test_data,
                         content_type='application/json')
        # Delete role
        resp = self.client.delete("/1",
                                  content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        # Delete non-existent role
        resp = self.client.delete("/1",
                                  content_type='application/json')
        self.assertEqual(resp.status_code, 404)
