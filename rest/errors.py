"""
Errors handlers for application.
"""

from flask import jsonify
from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException


def _exception_to_json(error):
    """Generate JSON HTTP response from Exception, HTTPException or other
    convertible to a string class.

    Make JSON response. If it is HTTPException - insert error.description to
    msg. If it is Exception or other class - insert str(error) type to msg.

    Args:
        error: HTTPException, Exception or convertible to a string class

    Return:
        JSON HTTP response
    """
    code = 500
    if isinstance(error, HTTPException):
        msg = error.description
        code = error.code
    else:
        msg = str(error)
    return jsonify({'error': msg}), code


def register_handlers(app):
    """Register global flask errorHandlers for all exceptions."""
    # Get all allow HTTP codes
    exceptions = [c for c, _ in default_exceptions.items()]
    # Add std and HTTP global exceptions if not debug mode
    if not app.debug:
        exceptions += [HTTPException, Exception]
    # Register handlers
    for exc in exceptions:
        app.errorhandler(exc)(_exception_to_json)
