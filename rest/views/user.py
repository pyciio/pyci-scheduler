"""
All /users views
"""

from datetime import datetime
from flask import abort, jsonify, request, g
from flask.views import MethodView
from flask_sqlalchemy_session import current_session
from sqlalchemy.exc import SQLAlchemyError
from rest.models.user import User
from rest.models.team import Team, Teams
from rest.models.role import Role, Roles

# pylint: disable=no-self-use


class UserView(MethodView):

    """
    Methods for add, delete, modify users.
    """

    def _access_control(self, user_id=None):
        """
        Check Role Based Access Control and client ID permissions.

        If client not administrator and if request user_id != current client ID
        """
        if "admin" in g.claims.get("scp"):
            return
        elif user_id is not None and g.claims.get("cid") == user_id:
            return
        else:
            abort(403)

    def get(self, user_id=None):
        """
        The method return user info.

        Return:
            - code 200 and list of all users from database if user_id not set.
            - code 200 and user by id if user_id set
            - code 404 if user not found by id
        """
        self._access_control(user_id)
        users = []
        if user_id is not None:
            user = current_session.query(User).filter_by(id=user_id).first()
            if user is None:
                abort(404, "user with id {0} not found".format(user_id))
            users.append(user.serialize)
        else:
            # only admins see all users
            for user in current_session.query(User).order_by(User.id):
                users.append(user.serialize)
        return jsonify(users=users)

    def post(self):
        """
        The method create new user.

        Parse request JSON body with required parameters 'username',
        'password', 'email' and create new user with role 'user'.

        Return:
            - code 404 if user already exist
            - code 201 if user create
        """
        # Only the administrator can add the new user
        self._access_control()
        body = request.get_json()
        username = body.get('username')
        password = body.get('password')
        email = body.get('email')
        if username is None or password is None or email is None:
            abort(400, "required parameters: username, password, email")
        user = User(username, password, email)
        role = current_session.query(Role).filter_by(name='user').first()
        roles = Roles(user=user, role=role)
        current_session.add(user)
        current_session.add(roles)
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "user already exist")
        return jsonify(msg="User {0} create".format(username)), 201

    def put(self, user_id):
        """
        The method changes the user's password or email or team.

        Required parameter 'user_id'. In JSON body to be set 'email' or
        'password'.

        Return:
            - code 404 if user not found
            - code 400 if user with that email exist
            - code 200 if user update
        """
        self._access_control(user_id)
        user = current_session.query(User).filter_by(id=user_id).first()
        if user is None:
            abort(404)
        body = request.get_json()
        if 'password' in body:
            user.password = body.get('password')
        if 'email' in body:
            user.email = body.get('email')
        if 'team' in body:
            team_name = body.get('team')
            team = current_session.query(
                Team).filter_by(name=team_name).first()
            if team is None:
                abort(404, "team with name '{0}' not found".format(team_name))
            teams = Teams(team=team, user=user)
            current_session.add(teams)
        user.last_modified = datetime.now()
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "user with that email exists")
        return jsonify(msg="User {0} update".format(user_id))

    def delete(self, user_id):
        """
        The method delete user from database.

        Required parameter 'user_id'.

        Return:
            - code 404 if user not found
            - code 200 if user delete
        """
        self._access_control(user_id)
        user = current_session.query(User).filter_by(id=user_id).first()
        if user is None:
            abort(404)
        current_session.delete(user)
        current_session.commit()
        return jsonify(msg="User {0} delete".format(user_id))
