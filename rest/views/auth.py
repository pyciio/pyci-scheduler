"""
All /authenticate views.
"""

from flask import jsonify
from flask import request
from flask.views import MethodView
from flask_sqlalchemy_session import current_session
from rest.models.user import User


class AuthView(MethodView):

    """
    Authenticate view. One method - POST, for authenticate new
    user and generate JWT token.
    """

    def __init__(self, jwt_auth):
        self.jwt_auth = jwt_auth

    def post(self):
        """
        The method authenticate and generate JWT token for user.

        Parse 'username' and 'password' from JSON request body and check
        user in database. Generate JWT token if credentials verified.

        Return:
            - code 401 if raise exception
            - code 401 if invalid credentials
            - code 200 if token generate
        """
        try:
            body = request.get_json()  # Get JSON POST data
            username = body.get('username')
            password = body.get('password')
            user = current_session.query(
                User).filter_by(login=username).first()
            if user is not None:
                if user.password == password:
                    roles = [p.role.name for p in user.roles.all()]
                    token = self.jwt_auth.get_user_token(user.id, roles)
                    return jsonify(token=token)
            # Use the "jsonify" instead of the function "abort" for the
            # interrupt request
            return jsonify(error="invalid credentials"), 401
        except:  # pylint: disable=bare-except
            # Use the "jsonify" instead of the function "abort" for the
            # interrupt request
            return jsonify(error="unauthorized"), 401
