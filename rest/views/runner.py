"""
All /runners views and /register view
"""

from flask import abort, jsonify, request
from flask.views import MethodView
from flask_sqlalchemy_session import current_session
from sqlalchemy.exc import IntegrityError
from rest.models.project import Project
from rest.models.runner import Runner

# pylint: disable=no-self-use


class RunnerView(MethodView):
    """
    Methods for get, delete, modify runners.
    """

    def get(self, runner_id=None):
        """
        The method return runner info.

        Return:
            - code 200 and list of all runners from database if runner_id not
              set.
            - code 200 and runner by id if runner_id set
            - code 404 if runner not found by id
        """
        runners = []
        if runner_id is not None:
            runner = current_session.query(
                Runner).filter_by(id=runner_id).first()
            if runner is None:
                abort(404, "runner with id {0} not found".format(runner_id))
            runners.append(runner.serialize)
        else:
            for runner in current_session.query(Runner).order_by(Runner.id):
                runners.append(runner.serialize)
        return jsonify(runners=runners)

    def put(self, runner_id):
        """
        The method changes the runner's name or description.

        Required parameter 'runner_id'. In JSON body to be set 'name' or
        'description' or both at once.

        Return:
            - code 404 if runner not found
            - code 400 if runner with that parameters exist
            - code 200 if runner update
        """
        runner = current_session.query(Runner).filter_by(id=runner_id).first()
        if runner is None:
            abort(404)
        body = request.get_json()
        if 'name' in body:
            runner.name = body.get('name')
        if 'ip' in body:
            runner.ip_address = body.get('ip')
        if 'executor' in body:
            runner.executor = body.get('executor')
        try:
            current_session.commit()
        except IntegrityError:
            abort(400, "bad parameters")
        return jsonify(msg="runner {0} update".format(runner_id))

    def delete(self, runner_id):
        """
        The method delete runner from database.

        Required parameter 'runner_id'.

        Return:
            - code 404 if runner not found
            - code 200 if runner delete
        """
        runner = current_session.query(Runner).filter_by(id=runner_id).first()
        if runner is None:
            abort(404)
        current_session.delete(runner)
        current_session.commit()
        return jsonify(msg="runner {0} delete".format(runner_id))


class RegisterRunnerView(MethodView):
    """
    Methods for register runners in project
    """

    def __init__(self, jwt_auth):
        self.jwt_auth = jwt_auth

    def post(self):
        """
        The method register new runner in project.

        Parse request JSON body with required parameters: 'project', 'token',
        'name', 'ip', 'executor' and register runner in project if project
        token validate.

        Return:
            - code 404 if project not found
            - code 400 if runner already register in this project
            - code 401 if bad project token
            - code 201 and JSON with runner's token if runner register
              in project
        """
        body = request.get_json()
        project_name = body.get('project')
        token = body.get('token')
        runner_name = body.get('name')
        runner_ip = body.get('ip')
        runner_executor = body.get('executor')
        if project_name is None:
            abort(400, "required parameters: project")
        if token is None:
            abort(400, "required parameters: token")
        if runner_name is None:
            abort(400, "required parameters: name")
        if runner_ip is None:
            abort(400, "required parameters: ip")
        if runner_executor is None:
            abort(400, "required parameters: executor")
        # Get project
        project_query = current_session.query(Project)
        project = project_query.filter_by(name=project_name).first()
        if project is None:
            abort(404, "project with name '{0}' not found".format(
                project_name))
        # Check project token
        if project.token != token:
            abort(401, "bad project token")
        # Create runner
        runner = Runner(runner_name, runner_ip, runner_executor)
        runner.token = ""
        runner.project = project
        # Add and flush for getting ID object
        current_session.add(runner)
        # Update database
        try:
            current_session.commit()
        except IntegrityError:
            abort(400, "runner already register in this project")
        # Add token
        runner_token = self.jwt_auth.get_runner_token(runner.id, project_name)
        runner.token = runner_token
        current_session.commit()
        return jsonify(msg="runner {0} register".format(runner_name),
                       token=runner_token), 201
