"""
Initialize views for application
"""

from rest.middleware import auth as jwt_auth
import rest.views.user
import rest.views.auth
import rest.views.team
import rest.views.role
import rest.views.project
import rest.views.build
import rest.views.runner


def register_handlers(app):
    """Register all views in Flask application"""
    # Init JWT middleware
    jwt = jwt_auth.JWTAuth(app.config.get('JWT_RSA_PRIV'),
                           app.config.get('JWT_RSA_PUB'))
    # Register handlers
    # User view
    user_view = rest.views.user.UserView.as_view('user_view')
    app.add_url_rule('%s/users' % app.config["API_PATH"],
                     view_func=user_view,
                     methods=['GET', 'POST'])
    app.add_url_rule('%s/users/<int:user_id>' % app.config["API_PATH"],
                     view_func=user_view,
                     methods=['GET', 'PUT', 'DELETE'])
    # Team view
    team_view = rest.views.team.TeamView.as_view('team_view')
    app.add_url_rule('%s/teams' % app.config["API_PATH"],
                     view_func=team_view,
                     methods=['GET', 'POST'])
    app.add_url_rule('%s/teams/<int:team_id>' % app.config["API_PATH"],
                     view_func=team_view,
                     methods=['GET', 'PUT', 'DELETE'])
    # Role view
    role_view = rest.views.role.RoleView.as_view('role_view')
    app.add_url_rule('%s/roles' % app.config["API_PATH"],
                     view_func=role_view,
                     methods=['GET', 'POST'])
    app.add_url_rule('%s/roles/<int:role_id>' % app.config["API_PATH"],
                     view_func=role_view,
                     methods=['GET', 'PUT', 'DELETE'])
    # Project view
    project_view = rest.views.project.ProjectView.as_view('project_view')
    app.add_url_rule('%s/projects' % app.config["API_PATH"],
                     view_func=project_view,
                     methods=['GET', 'POST'])
    app.add_url_rule('%s/projects/<int:project_id>' % app.config["API_PATH"],
                     view_func=project_view,
                     methods=['GET', 'PUT', 'DELETE'])
    # Build view
    build_view = rest.views.build.BuildView.as_view('build_view')
    app.add_url_rule('%s/builds' % app.config["API_PATH"],
                     view_func=build_view,
                     methods=['GET', 'POST'])
    app.add_url_rule('%s/builds/<int:build_id>' % app.config["API_PATH"],
                     view_func=build_view,
                     methods=['GET', 'PUT', 'DELETE'])
    # Runner view
    runner_view = rest.views.runner.RunnerView.as_view('runner_view')
    app.add_url_rule('%s/runners' % app.config["API_PATH"],
                     view_func=runner_view,
                     methods=['GET', 'POST'])
    app.add_url_rule('%s/runners/<int:runner_id>' % app.config["API_PATH"],
                     view_func=runner_view,
                     methods=['GET', 'PUT', 'DELETE'])
    # RegisterRunner view
    reg_runner_view = rest.views.runner.RegisterRunnerView.as_view(
        "reg_runner_view", jwt_auth=jwt)
    app.add_url_rule('%s/register' % app.config["API_PATH"],
                     view_func=reg_runner_view,
                     methods=['POST'])
    # Authenticate
    app.add_url_rule('%s/authenticate' % app.config["API_PATH"],
                     view_func=rest.views.auth.AuthView.as_view(
                         "auth_view", jwt_auth=jwt),
                     methods=['POST'])
