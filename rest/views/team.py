"""
All /teams views
"""

from flask import abort, jsonify, request
from flask.views import MethodView
from flask_sqlalchemy_session import current_session
from sqlalchemy.exc import SQLAlchemyError
from rest.models.team import Team

# pylint: disable=no-self-use


class TeamView(MethodView):
    """
    Methods for add, delete, modify teams.
    """

    def get(self, team_id=None):
        """
        The method return team info.

        Return:
            - code 200 and list of all teams from database if team_id not set.
            - code 200 and team by id if team_id set
            - code 404 if team not found by id
        """
        teams = []
        if team_id is not None:
            team = current_session.query(Team).filter_by(id=team_id).first()
            if team is None:
                abort(404, "team with id {0} not found".format(team_id))
            teams.append(team.serialize)
        else:
            for team in current_session.query(Team).order_by(Team.id):
                teams.append(team.serialize)
        return jsonify(teams=teams)

    def post(self):
        """
        The method create new team.

        Parse request JSON body with required parameter 'name' and create new
        team.
        Optional parameters in JSON body: 'description'.

        Return:
            - code 404 if team with same name already exist
            - code 201 if team create
        """
        body = request.get_json()
        team_name = body.get('name')
        team_descr = body.get('description')
        if team_name is None:
            abort(400, "required parameters: name")
        team = Team(team_name, team_descr)
        current_session.add(team)
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "team already exist")
        return jsonify(msg="team {0} create".format(team_name)), 201

    def put(self, team_id):
        """
        The method changes the team's name or description.

        Required parameter 'team_id'. In JSON body to be set 'name' or
        'description' or both at once.

        Return:
            - code 404 if team not found
            - code 400 if team with that name exist
            - code 200 if team update
        """
        team = current_session.query(Team).filter_by(id=team_id).first()
        if team is None:
            abort(404)
        body = request.get_json()
        if 'name' in body:
            team.name = body.get('name')
        if 'description' in body:
            team.description = body.get('description')
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "team with same name already exists")
        return jsonify(msg="team {0} update".format(team_id))

    def delete(self, team_id):
        """
        The method delete team from database.

        Required parameter 'team_id'.

        Return:
            - code 404 if team not found
            - code 200 if team delete
        """
        team = current_session.query(Team).filter_by(id=team_id).first()
        if team is None:
            abort(404)
        current_session.delete(team)
        current_session.commit()
        return jsonify(msg="team {0} delete".format(team_id))
