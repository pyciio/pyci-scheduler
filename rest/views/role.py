"""
All /roles views
"""

from flask import abort, jsonify, request
from flask.views import MethodView
from flask_sqlalchemy_session import current_session
from sqlalchemy.exc import SQLAlchemyError
from rest.models.role import Role

# pylint: disable=no-self-use


class RoleView(MethodView):

    """
    Methods for add, delete, modify roles.
    """

    def get(self, role_id=None):
        """
        The method return role info.

        Return:
            - code 200 and list of all roles from database if role_id not set.
            - code 200 and role by id if role_id set
            - code 404 if role not found by id
        """
        roles = []
        if role_id is not None:
            role = current_session.query(Role).filter_by(id=role_id).first()
            if role is None:
                abort(404, "role with id {0} not found".format(role_id))
            roles.append(role.serialize)
        else:
            for role in current_session.query(Role).order_by(Role.id):
                roles.append(role.serialize)
        return jsonify(roles=roles)

    def post(self):
        """
        The method create new role.

        Parse request JSON body with required parameter 'name' and create new
        role.
        Optional parameters in JSON body: 'description'.

        Return:
            - code 404 if role with same name already exist
            - code 201 if role create
        """
        body = request.get_json()
        role_name = body.get('name')
        role_descr = body.get('description')
        if role_name is None:
            abort(400, "required parameters: name")
        role = Role(role_name, role_descr)
        current_session.add(role)
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "role already exist")
        return jsonify(msg="role {0} create".format(role_name)), 201

    def put(self, role_id):
        """
        The method changes the role's name or description.

        Required parameter 'role_id'. In JSON body to be set 'name' or
        'description' or both at once.

        Return:
            - code 404 if role not found
            - code 400 if role with that name exist
            - code 200 if role update
        """
        role = current_session.query(Role).filter_by(id=role_id).first()
        if role is None:
            abort(404)
        body = request.get_json()
        if 'name' in body:
            role.name = body.get('name')
        if 'description' in body:
            role.description = body.get('description')
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "role with same name already exists")
        return jsonify(msg="role {0} update".format(role_id))

    def delete(self, role_id):
        """
        The method delete role from database.

        Required parameter 'role_id'.

        Return:
            - code 404 if role not found
            - code 200 if role delete
        """
        role = current_session.query(Role).filter_by(id=role_id).first()
        if role is None:
            abort(404)
        current_session.delete(role)
        current_session.commit()
        return jsonify(msg="role {0} delete".format(role_id))
