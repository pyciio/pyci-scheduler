"""
All /projects views
"""

from flask import abort, jsonify, request
from flask.views import MethodView
from flask_sqlalchemy_session import current_session
from sqlalchemy.exc import SQLAlchemyError
from rest.models.team import Team
from rest.models.project import Project

# pylint: disable=no-self-use


class ProjectView(MethodView):
    """
    Methods for add, delete, modify projects.
    """

    def get(self, project_id=None):
        """
        The method return project info.

        Return:
            - code 200 and list of all projects from database if project_id
              not set.
            - code 200 and project by id if project_id set
            - code 404 if project not found by id
        """
        projects = []
        project_query = current_session.query(Project)
        if project_id is not None:
            project = project_query.filter_by(id=project_id).first()
            if project is None:
                abort(404, "project with id {0} not found".format(project_id))
            projects.append(project.serialize)
        else:
            for project in project_query.order_by(Project.id):
                projects.append(project.serialize)
        return jsonify(projects=projects)

    def post(self):
        """
        The method create new project.

        Parse request JSON body with required parameters: 'name', 'repository',
        'team' and create new project.

        Return:
            - code 404 if project with same name already exist
            - code 201 if project create
        """
        body = request.get_json()
        project_name = body.get('name')
        project_repo = body.get('repository')
        project_team = body.get('team')
        if project_name is None:
            abort(400, "required parameters: name")
        if project_repo is None:
            abort(400, "required parameters: repository")
        if project_team is None:
            abort(400, "required parameters: team")
        team = current_session.query(Team).filter_by(name=project_team).first()
        if team is None:
            abort(404, "team with name '{0}' not found".format(project_team))
        current_session.add(Project(project_name, project_repo, team))
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "project already exist")
        return jsonify(msg="project {0} create".format(project_name)), 201

    def put(self, project_id):
        """
        The method changes the project.

        Required parameter 'project_id'. In JSON body to be set 'name' or
        'repository' or 'team'.

        Return:
            - code 404 if project not found
            - code 400 if project with that name exist
            - code 200 if project update
        """
        project_query = current_session.query(Project)
        project = project_query.filter_by(id=project_id).first()
        if project is None:
            abort(404)
        body = request.get_json()
        if 'name' in body:
            project.name = body.get('name')
        if 'repository' in body:
            project.repository = body.get('repository')
        if 'team' in body:
            team_name = body.get('team')
            team = current_session.query(
                Team).filter_by(name=team_name).first()
            if team is None:
                abort(404, "team with name '{0}' not found".format(team_name))
            project.team = team
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "project with same name already exists")
        return jsonify(msg="project {0} update".format(project_id))

    def delete(self, project_id):
        """
        The method delete project from database.

        Required parameter 'project_id'.

        Return:
            - code 404 if project not found
            - code 200 if project delete
        """
        project_query = current_session.query(Project)
        project = project_query.filter_by(id=project_id).first()
        if project is None:
            abort(404)
        current_session.delete(project)
        current_session.commit()
        return jsonify(msg="project {0} delete".format(project_id))
