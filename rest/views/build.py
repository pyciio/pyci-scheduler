"""
All /builds views
"""

from flask import abort, jsonify, request, g
from flask.views import MethodView
from flask_sqlalchemy_session import current_session
from sqlalchemy.exc import SQLAlchemyError
from rest.models.project import Project
from rest.models.build import Build

# pylint: disable=no-self-use


class BuildView(MethodView):

    """
    Methods for add, delete, modify builds.
    """

    def _access_control(self, project):
        """
        Check Role Based Access Control and client ID permissions.

        If client not administrator and if request user_id != current client ID
        """
        cid = g.claims.get("cid")
        if "admin" in g.claims.get("scp"):
            return
        elif "user" in g.claims.get("scp"):
            # check user in project's team
            if cid in [t.user.id for t in project.team.teams]:
                return
        elif "runner" in g.claims.get("scp"):
            # runner can only get or update build
            if request.method in ['GET', 'PUT']:
                # check runner in project's team
                if cid in [r.id for r in project.runners]:
                    return
        abort(403)

    def get(self, build_id=None):
        """
        The method return build info.

        Return:
            - code 200 and list of all builds from database if build_id
              not set.
            - code 200 and build by id if build_id set
            - code 404 if build not found by id
        """
        builds = []
        build_query = current_session.query(Build)
        if build_id is not None:
            build = build_query.filter_by(id=build_id).first()
            if build is None:
                abort(404, "build with id {0} not found".format(build_id))
            self._access_control(build.project)
            builds.append(build.serialize)
        else:
            # only administrator view all builds
            if "admin" not in g.claims.get("scp"):
                abort(403)
            for build in build_query.order_by(Build.id):
                builds.append(build.serialize)
        return jsonify(builds=builds)

    def post(self):
        """
        The method create new build.

        Parse request JSON body with required parameters: 'name', 'project',
        'config', 'commit_hash' and create new build.

        Return:
            - code 404 if build with same name already exist
            - code 201 if build create
        """
        body = request.get_json()
        build_name = body.get('name')
        build_project = body.get('project')
        if build_name is None:
            abort(400, "required parameters: name")
        if build_project is None:
            abort(400, "required parameters: project")
        if body.get('config') is None:
            abort(400, "required parameters: config")
        if body.get('commit_hash') is None:
            abort(400, "required parameters: commit_hash")
        project_query = current_session.query(Project)
        project = project_query.filter_by(name=build_project).first()
        if project is None:
            abort(404, "project with name '{0}' not found".format(
                build_project))
        self._access_control(project)
        current_session.add(Build(build_name, project, body.get('config'),
                                  body.get('commit_hash')))
        try:
            current_session.commit()
        except SQLAlchemyError:
            abort(400, "build already exist")
        return jsonify(msg="build {0} create".format(build_name)), 201

    def put(self, build_id):
        """
        The method changes the build.

        Required parameter 'build_id'. In JSON body to be set 'status' or
        'logs'.

        Return:
            - code 404 if build not found
            - code 400 if build with that name exist
            - code 200 if build update
        """
        build_query = current_session.query(Build)
        build = build_query.filter_by(id=build_id).first()
        if build is None:
            abort(404)
        self._access_control(build.project)
        body = request.get_json()
        if 'status' in body:
            build.update_status(body.get('status'))
        if 'logs' in body:
            build.logs = body.get('logs')
        try:
            current_session.commit()
        except SQLAlchemyError as err:
            abort(400, "build update error: {0}".format(err))
        return jsonify(msg="build {0} update".format(build_id))

    def delete(self, build_id):
        """
        The method delete build from database.

        Required parameter 'build_id'.

        Return:
            - code 404 if build not found
            - code 200 if build delete
        """
        build_query = current_session.query(Build)
        build = build_query.filter_by(id=build_id).first()
        if build is None:
            abort(404)
        self._access_control(build.project)
        current_session.delete(build)
        current_session.commit()
        return jsonify(msg="build {0} delete".format(build_id))
