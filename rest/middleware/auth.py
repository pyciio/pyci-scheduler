"""
Middleware for manage JWT tokens.
"""
import time
import logging
import jwt

LOG = logging.getLogger('pyci-scheduler-api')


class JWTAuthError(Exception):
    """Exception for JWTAuth class"""
    pass


class JWTAuth(object):
    """
    Class for manage JWT auth tokens.

    Args:
        rsa_priv - RSA private key file
        rsa_pub - RSA public key file
        expire - expire time for tokens
    """

    def __init__(self, rsa_priv, rsa_pub, expire=3600):
        # Load RSA keys
        self.priv_key = None
        self.pub_key = None
        self.expire = expire
        with open(rsa_priv, 'r') as key:
            self.priv_key = key.read()
        with open(rsa_pub, 'r') as key:
            self.pub_key = key.read()

    def check_token(self, token):
        """
        Validate token.

        Parse token string and check JWT claims.

        Raise exception JWTAuthError if token is invalid.
        """
        try:
            options = {
                'require_exp': True,
                'require_iat': True,
            }
            return jwt.decode(jwt=token, key=self.pub_key,
                              verify=True, options=options)
        except jwt.ExpiredSignatureError:
            raise JWTAuthError("Token has expired")
        except jwt.InvalidTokenError:
            raise JWTAuthError("Token is invalid")
        except Exception as err:
            LOG.error("Unhandled exception in check_token(): %s", str(err))
            raise JWTAuthError("Unauthorized")

    def get_user_token(self, user_id, roles):
        """
        Generate new JWT token with algorithm RS512 for user.

        Return token string.
        """
        # Create payload (claims)
        jwt_claims = {
            "cid": user_id,
            "scp": roles,
            "exp": int(time.time()) + self.expire,
            "iat": int(time.time()),
            # "jti": "jwt_id"  # TODO: jwt token ID
        }
        return jwt.encode(payload=jwt_claims,
                          key=self.priv_key,
                          algorithm='RS512').decode('utf-8')

    def get_runner_token(self, runner_id, project):
        """
        Generate new JWT token with algorithm RS512 for runner.

        Return token string.
        """
        # Create payload (claims)
        jwt_claims = {
            "cid": runner_id,
            "scp": ["runner"],
            "exp": int(time.time()) + 31536000,  # one year expire
            "iat": int(time.time()),
            "project": project,
        }
        return jwt.encode(payload=jwt_claims,
                          key=self.priv_key,
                          algorithm='RS512').decode('utf-8')
