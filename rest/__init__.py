#!/usr/bin/env python

"""
RESTful API module.

It contains:
    - flask application initilize function (.create_app())
    - database models (rest.models)
    - views (rest.views)
    - error handlers (rest.errors)
    - before request handlers (rest.before)
    - middlewares (rest.middleware)

Example usage:
    import rest
    app = rest.create_app()
    app.run()

For debugging setup enviroment variable PYCI_DEV=1.
"""

import os
from flask import Flask
from flask_sqlalchemy_session import flask_scoped_session
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# ====> error handlers
import rest.errors
# ====> before handlers
import rest.before
# ====> views
import rest.views


def create_app():
    """
    Initilize flask application.

    Create flask application with config.from_object(), crate database
    connection and register all types handlers.

    Return flask application.
    """
    # Create flask 'app' object
    app = Flask(__name__)
    # Check debug env
    dev_mode = os.getenv('PYCI_DEV', False)
    if dev_mode:
        app.config.from_object('config.DevelopmentConfig')
    else:
        app.config.from_object('config.ProductionConfig')

    # Create session for database
    # http://flask-sqlalchemy-session.readthedocs.io/en/v1.1/
    # http://docs.sqlalchemy.org/en/rel_0_9/orm/contextual.html#using-custom-created-scopes
    db_engine = create_engine(app.config.get('SQLALCHEMY_DATABASE_URI'),
                              echo=False, convert_unicode=True)
    session_factory = sessionmaker(bind=db_engine, autocommit=False)
    flask_scoped_session(session_factory, app)
    # session = scoped_session(sessionmaker(bind=db_engine, autocommit=False))
    # models.Base.query = session.query_property()

    # Register errors handlers
    rest.errors.register_handlers(app)
    # Register 'before_request' handlers
    rest.before.register_handlers(app)
    # Register views
    rest.views.register_handlers(app)

    return app

APP = create_app()
