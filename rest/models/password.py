"""
Password objects in database

The code is taken from here:
http://variable-scope.com/posts/storing-and-verifying-passwords-with-sqlalchemy
and modified to support all versions of python.
"""

import bcrypt
import six
from sqlalchemy import Text
import sqlalchemy.types as types


class PasswordHash(object):  # pylint: disable=too-few-public-methods

    """Hashed password object."""

    def __init__(self, hash_):
        # if python 3 - decode from bytes to str
        if six.PY3 and isinstance(hash_, bytes):
            hash_ = hash_.decode()
        self.hash = hash_
        assert len(hash_) == 60, 'bcrypt hash should be 60 chars.'
        assert hash_.count('$'), 'bcrypt hash should have 3x "$".'
        self.rounds = int(hash_.split('$')[2])

    def __eq__(self, candidate):
        """Hashes the candidate string and compares it to the stored hash."""
        if isinstance(candidate, six.string_types):
            if isinstance(candidate, six.text_type):
                candidate = candidate.encode('utf8')
            return bcrypt.checkpw(candidate, self.hash.encode())
        return False

    def __repr__(self):
        """Simple object representation."""
        return '<{0}>'.format(type(self).__name__)

    @classmethod
    def new(cls, password, rounds):
        """Creates a PasswordHash from the given password."""
        if isinstance(password, six.text_type):
            password = password.encode('utf8')
        return cls(bcrypt.hashpw(password, bcrypt.gensalt(rounds)))


class Password(types.TypeDecorator):

    """Allows storing and retrieving password hashes using PasswordHash."""
    impl = Text
    python_type = None
    process_literal_param = None

    def __init__(self, rounds=12, **kwds):
        self.rounds = rounds
        # print("Type:", self.python_type)
        super(Password, self).__init__(**kwds)

    def process_bind_param(self, value, dialect):
        """Ensure the value is a PasswordHash and then return its hash."""
        return self._convert(value).hash

    def process_result_value(self, value, dialect):
        """Convert the hash to a PasswordHash, if it's non-NULL."""
        if value is not None:
            return PasswordHash(value)

    def validator(self, password):
        """Provides a validator/converter for @validates usage."""
        return self._convert(password)

    def _convert(self, value):
        """Returns a PasswordHash from the given string.

        PasswordHash instances or None values will return unchanged.
        Strings will be hashed and the resulting PasswordHash returned.
        Any other input will result in a TypeError.
        """
        if isinstance(value, PasswordHash):
            return value
        elif isinstance(value, six.string_types):
            return PasswordHash.new(value, self.rounds)
        elif value is not None:
            raise TypeError("Cannot convert {0} to a "
                            "PasswordHash".format(type(value)))
