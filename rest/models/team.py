"""
Database team model
"""
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship
from rest.models import Base


class Team(Base):  # pylint: disable=too-few-public-methods
    """'team' database table model"""
    __tablename__ = 'team'
    id = Column(Integer, primary_key=True)  # pylint: disable=invalid-name
    name = Column(String(20), index=True, unique=True, nullable=False)
    description = Column(String(140))
    teams = relationship('Teams', backref='team', lazy='dynamic')
    project = relationship('rest.models.project.Project',
                           backref='team', lazy='dynamic')

    def __repr__(self):
        return "<Team '{0}'>".format(self.name)

    def __init__(self, name_, description_=None):
        self.name = name_
        if description_ is None:
            description_ = ""
        self.description = description_

    @property
    def serialize(self):
        """Serialize team object to string for JSON response"""
        members = [t.user.login for t in self.teams]
        return {"name": self.name,
                "description": self.description,
                "members": members}


class Teams(Base):  # pylint: disable=too-few-public-methods
    """'teams' database table model"""
    __tablename__ = 'teams'
    id = Column(Integer, primary_key=True)  # pylint: disable=invalid-name
    team_id = Column(Integer, ForeignKey('team.id'))
    # team
    user_id = Column(Integer, ForeignKey('user.id'))
    # user
    __table_args__ = (UniqueConstraint(
        'team_id', 'user_id', name='_teams_uc'),)

    def __repr__(self):
        return "<Teams '{0}'>".format(self.id)
