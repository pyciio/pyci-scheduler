"""
Database build model
"""
import json
from datetime import datetime
from sqlalchemy import Column, Integer, String, ForeignKey, \
    DateTime, Text, Enum
from rest.models import Base

# pylint: disable=too-few-public-methods,too-many-instance-attributes


class Build(Base):

    """'build' database table model"""
    __tablename__ = 'build'
    id = Column(Integer, primary_key=True)  # pylint: disable=invalid-name
    name = Column(String(64), index=True, unique=True, nullable=False)
    config = Column(Text)
    logs = Column(Text)
    create = Column(DateTime)
    started = Column(DateTime)
    ended = Column(DateTime)
    status = Column(Enum("successful", "failed",
                         "stoped", "wait", "new", "run"))
    project_id = Column(Integer, ForeignKey('project.id'), nullable=False)
    # project
    commit_message = Column(Text)
    commit_hash = Column(String(40))
    commit_branch = Column(String(100))
    commit_author = Column(String(64))

    def __repr__(self):
        return "<Build '{0}'>".format(self.name)

    # pylint: disable=too-many-arguments
    def __init__(self, name_, project_, config_, hash_,
                 branch_="master", author_="", message_=""):
        self.name = name_
        self.project = project_
        self.config = json.dumps(config_)
        self.create = datetime.now()
        self.status = "new"
        self.commit_branch = branch_
        self.commit_hash = hash_
        self.commit_author = author_
        self.commit_message = message_

    @property
    def serialize(self):
        """Serialize project object to string for JSON response"""
        return {"id": self.id,
                "name": self.name,
                "repository": self.project.repository,
                "create_date": self.create,
                "started_date": self.started,
                "ended_date": self.ended,
                "config": json.loads(self.config),
                "logs": self.logs,
                "status": self.status,
                "commit": {
                    "hash": self.commit_hash,
                    "branch": self.commit_branch,
                    "author": self.commit_author,
                    "message": self.commit_message
                }}

    def update_status(self, new_status):
        """Update build status"""
        cur_status = self.status
        if cur_status != new_status:
            if new_status == "run":
                self.started = datetime.now()
            elif new_status in ["successful", "failed", "stoped"]:
                self.ended = datetime.now()
            self.status = new_status
