"""
Database runner model
"""
from datetime import datetime
from sqlalchemy import Column, Integer, String, ForeignKey, \
    UniqueConstraint, DateTime, Enum, Text
from rest.models import Base
from rest.models.ipaddr import IPAddress


class Runner(Base):  # pylint: disable=too-few-public-methods
    """'runner' database table model"""
    __tablename__ = 'runner'
    id = Column(Integer, primary_key=True)  # pylint: disable=invalid-name
    name = Column(String(20), index=True, nullable=False)
    create = Column(DateTime)
    ip_address = Column(IPAddress, nullable=False)
    executor = Column(Enum("docker", "shell"))
    token = Column(Text)
    project_id = Column(Integer, ForeignKey('project.id'), nullable=False)
    # project
    __table_args__ = (UniqueConstraint(
        'name', 'project_id', name='_runner_uc'),)

    def __repr__(self):
        return "<Runner '{0}'>".format(self.name)

    def __init__(self, name_, ip_address_, executor_):
        self.name = name_
        self.create = datetime.now()
        self.ip_address = ip_address_
        self.executor = executor_

    @property
    def serialize(self):
        """Serialize runner object to string for JSON response"""
        return {"id": self.id,
                "name": self.name,
                "create_datetime": self.create,
                "ip": self.ip_address,
                "executor": self.executor,
                "token": self.token,
                "project": self.project.name}
