"""
Database project model
"""
import os
import binascii
from datetime import datetime
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from rest.models import Base


class Project(Base):  # pylint: disable=too-few-public-methods
    """'project' database table model"""
    __tablename__ = 'project'
    id = Column(Integer, primary_key=True)  # pylint: disable=invalid-name
    name = Column(String(64), index=True, unique=True, nullable=False)
    repository = Column(String(120), index=True, unique=True, nullable=False)
    create = Column(DateTime)
    team_id = Column(Integer, ForeignKey('team.id'), nullable=False)
    token = Column(String(30), nullable=False)
    archive_path = Column(String(255), server_default="")
    # team
    builds = relationship('rest.models.build.Build',
                          backref='project', lazy='dynamic')
    runners = relationship('rest.models.runner.Runner',
                           backref='project', lazy='dynamic')

    def __repr__(self):
        return "<Project '{0}'>".format(self.name)

    def __init__(self, name_, repo_, team_):
        self.name = name_
        self.repository = repo_
        self.team = team_
        self.create = datetime.now()
        self.token = binascii.b2a_hex(os.urandom(15)).decode('utf8')

    @property
    def serialize(self):
        """Serialize project object to string for JSON response"""
        members = [t.user.login for t in self.team.teams]
        runners = [r.name for r in self.runners]
        return {"id": self.id,
                "name": self.name,
                "repository": self.repository,
                "create_datetime": self.create,
                "team": self.team.name,
                "members": members,
                "runners": runners,
                "token": self.token,
                "archive": self.archive_path}
