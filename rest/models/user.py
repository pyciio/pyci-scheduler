"""
Database user model
"""
from datetime import datetime
from sqlalchemy import Column, Integer, String, Enum, DateTime
from sqlalchemy.orm import validates, relationship
from rest.models import Base
from rest.models.password import Password


class User(Base):  # pylint: disable=too-few-public-methods
    """'user' database table model"""
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)  # pylint: disable=invalid-name
    login = Column(String(64), index=True, unique=True, nullable=False)
    password = Column(Password)
    email = Column(String(120), index=True, unique=True, nullable=False)
    status = Column(Enum("verified", "unverified"))
    create = Column(DateTime)
    last_modified = Column(DateTime)
    roles = relationship('rest.models.role.Roles',
                         backref='user', lazy='dynamic')
    teams = relationship('rest.models.team.Teams',
                         backref='user', lazy='dynamic')

    def __repr__(self):
        return "<User '{0}'>".format(self.login)

    def __init__(self, login_, password_, email_):
        self.login = login_
        self.password = password_
        self.email = email_
        self.status = "unverified"
        self.create = datetime.now()
        self.last_modified = datetime.now()

    @validates('password')
    def _validate_password(self, key, password):
        return getattr(type(self), key).type.validator(password)

    @property
    def serialize(self):
        """Serialize role object to string for JSON response"""
        roles = [p.role.name for p in self.roles.all()]
        teams = [t.team.name for t in self.teams.all()]
        return {"id": self.id,
                "login": self.login,
                "email": self.email,
                "status": self.status,
                "create_datetime": self.create,
                "last_modified": self.last_modified,
                "roles": roles,
                "teams": teams}
