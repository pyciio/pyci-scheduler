"""
IP address objects in database.

CIDR ip adrress to be convert to integer/
"""

import socket
import struct
import six
from sqlalchemy import Integer
import sqlalchemy.types as types


class IPAddress(types.TypeDecorator):
    """Allows storing and retrieving integer."""
    impl = Integer
    python_type = None
    process_literal_param = None

    def __init__(self, **kwargs):
        super(IPAddress, self).__init__(**kwargs)

    def process_bind_param(self, value, dialect):
        """Return integer from ip CIDR"""
        return self._convert(value)

    def process_result_value(self, value, dialect):
        """Convert integer to ip CIDR if it's not None"""
        if value is not None:
            return self._int_to_ip(value)

    @staticmethod
    def _ip_to_int(addr):
        """Convert ip CIDR to integer"""
        try:
            return struct.unpack("!I", socket.inet_aton(addr))[0]
        except socket.error as err:
            raise TypeError(
                "Cannot convert {0} to a integer: {1}".format(addr, err))

    @staticmethod
    def _int_to_ip(addr):
        """Convert integer to ip CIDR"""
        try:
            return socket.inet_ntoa(struct.pack("!I", addr))
        except struct.error as err:
            raise TypeError(
                "Cannot convert {0} to a ip CIDR: {1}".format(addr, err))

    def _convert(self, addr):
        """Returns a integer if necessary.

        Integer instances are checked for belonging to the range 0-4294967295
        and return unchanged. If out of range raise a TypeError.
        None values will return unchanged.
        Strings will be convert and the resulting integer returned.
        Any other input will result in a TypeError.
        """
        if isinstance(addr, six.integer_types):
            if addr < 0 or addr > 4294967295:
                raise TypeError("Out of range integer ip address")
            return addr
        elif isinstance(addr, six.string_types):
            return self._ip_to_int(addr)
        elif addr is not None:
            raise TypeError("Cannot convert {0} to a "
                            "IPAddress".format(type(addr)))
