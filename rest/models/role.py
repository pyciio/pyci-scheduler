"""
Database role model
"""
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship
from rest.models import Base


class Role(Base):  # pylint: disable=too-few-public-methods
    """'role' database table model"""
    __tablename__ = 'role'
    id = Column(Integer, primary_key=True)  # pylint: disable=invalid-name
    name = Column(String(20), index=True, unique=True, nullable=False)
    description = Column(String(140))
    roles = relationship('Roles', backref='role', lazy='dynamic')

    def __repr__(self):
        return "<Role '{0}'>".format(self.name)

    def __init__(self, name_, description_=None):
        self.name = name_
        if description_ is None:
            description_ = ""
        self.description = description_

    @property
    def serialize(self):
        """Serialize role object to string for JSON response"""
        return {"name": self.name,
                "description": self.description}


class Roles(Base):  # pylint: disable=too-few-public-methods
    """'roles' database table model"""
    __tablename__ = 'roles'
    id = Column(Integer, primary_key=True)  # pylint: disable=invalid-name
    role_id = Column(Integer, ForeignKey('role.id'))
    # role
    user_id = Column(Integer, ForeignKey('user.id'))
    # user
    __table_args__ = (UniqueConstraint(
        'role_id', 'user_id', name='_roles_uc'),)

    def __repr__(self):
        return "<Roles '{0}'>".format(self.id)
