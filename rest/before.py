"""
Before request application handlers
"""

import flask
from flask import jsonify
from flask import request
# Import JWT functions
from rest.middleware import auth


def register_handlers(app):
    """Wrapper function for register before request handlers"""
    api_path = app.config["API_PATH"]
    # Init JWT middleware
    jwt_auth = auth.JWTAuth(app.config.get('JWT_RSA_PRIV'),
                            app.config.get('JWT_RSA_PUB'))

    @app.before_request
    def _check_jwt():  # pylint: disable=unused-variable
        """
        Handler for authorize request.

        Parse 'Authorization' header. Parse and check token string.

        Return:
            - code 401 if authorization error
            - None (next handler) if not authorization error
        """
        # If we aren't requesting a token, enforce authorization
        if request.path == "{0}/authenticate".format(api_path):
            return
        elif request.path == "{0}/register".format(api_path):
            return
        else:
            try:
                # Token in the format Authorization: Bearer
                auth_header = request.headers.get("Authorization")
                if auth_header is not None and "Bearer " in auth_header:
                    # We just need the token after Bearer
                    token = auth_header.split(' ')[1]
                    flask.g.claims = jwt_auth.check_token(token)
                else:
                    return jsonify(error="Bad header. Please use: "
                                         "Authorization Bearer <TOKEN>"), 401
            except auth.JWTAuthError as err:
                return jsonify(error=str(err)), 401
