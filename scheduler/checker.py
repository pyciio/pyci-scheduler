"""
Project checker module.

The class is run in a separate process and checks for updates in the project.
"""

import os
import logging
import time
import shutil
import git
import yaml
from sqlalchemy.orm import sessionmaker
from rest.models.project import Project
from rest.models.build import Build

LOG = logging.getLogger('pyci-scheduler')


class ProjectChecker(object):  # pylint: disable=too-few-public-methods
    """
    Checker class.

    Run in separate process with context manager and checks for updates in the
    project. The main function run() of the class checks the existence of the
    directory with the repository, synchronizes the history with the remote
    repository and if there were changes creates new builds for the runners.
    """

    def __init__(self, db_engine_, project_id_, archive_):
        # will ensure any remaining connections are flushed.
        # http://docs.sqlalchemy.org/en/latest/faq/connections.html#how-do-i-use-engines-connections-sessions-with-python-multiprocessing-or-os-fork
        db_engine_.dispose()
        self.db_engine = db_engine_
        self.project_id = project_id_
        self.archive = archive_
        # Make in context
        self.sess = None
        self.project = None

    def __enter__(self):
        # Database connection
        sess_maker = sessionmaker(bind=self.db_engine, autocommit=False)
        self.sess = sess_maker()
        # Prepare project
        project_query = self.sess.query(Project)
        self.project = project_query.filter_by(id=self.project_id).first()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # LOG.debug("Call ProjectChecker.__exit__()")
        self.sess.close()

    def _create_archive(self):
        """Create archive directory if it not exist"""
        archive_dir = os.path.join(self.archive, self.project.name)
        # Create archive dir
        try:
            os.makedirs(archive_dir)
        except OSError as err:
            shutil.rmtree(archive_dir)
        LOG.debug("Create archive for new repository %s", archive_dir)
        # Pull not exist repo
        try:
            repo = git.Repo.clone_from(self.project.repository, archive_dir)
        except git.exc.GitCommandError as err:
            LOG.error("Clone new repository error: %s", err)
            return False
        # First sync repo heads
        self._sync_heads(repo)
        # Update database
        self.project.archive_path = archive_dir
        self.sess.commit()
        return True

    def _load_config(self):
        """Load YAML config file (.pyci.yml) from main branch repository"""
        yaml_file = os.path.join(self.project.archive_path, ".pyci.yml")
        if not os.path.exists(yaml_file):
            LOG.error("Config file %s not exist", yaml_file)
            return
        with open(yaml_file) as pyci_file:
            try:
                return yaml.load(pyci_file)
            except yaml.YAMLError as err:
                LOG.error("Config file %s parse error: %s", yaml_file, err)

    def _check_update(self, depth=50, new_archive=False):
        """Check updates in remote repository"""
        repo = git.Repo(self.project.archive_path)
        last_commit = {}
        # Store last commits
        for head in repo.heads:
            for commit in repo.iter_commits(rev=head, max_count=1):
                last_commit[head.name] = commit
        if new_archive:
            return last_commit
        # Sync repo heads
        if not self._sync_heads(repo):
            return {}
        # Get new commits from branches and check whether it is new or not
        new_commits = {}
        for head in repo.heads:
            new_commits[head.name] = []
            for commit in repo.iter_commits(rev=head, max_count=depth):
                # Check new branch
                if head.name in last_commit:
                    # Check commit in old branch
                    if last_commit[head.name] == commit:
                        break
                # Add new commit
                new_commits[head.name].append(commit)
        return new_commits

    @staticmethod
    def _sync_heads(repo):
        """Synchronize local and remote branches (heads)."""
        remote_heads = []
        # Checking new remote branches
        # TODO: pull from all remotes
        try:
            # Add new remote branches to local
            for info in repo.remotes.origin.pull(prune=True):
                remote_heads.append(info.ref.remote_head)
                if info.ref.remote_head not in repo.heads:
                    LOG.info("Found new branch %s in remote, create local "
                             "ref '%s'", info.ref.name, info.ref.remote_head)
                    repo.create_head(info.ref.remote_head, info.ref)
            # Removing branches that do not exist remotely
            for head in repo.heads:
                if head.name not in remote_heads:
                    LOG.info("Found branch not exist in the remote "
                             "repository '%s', delete it", head.name)
                    repo.git.branch('-D', head.name)
            return True
        except git.exc.GitCommandError as err:
            LOG.error("Git pull repo error: %s", err)
        return False

    def run(self):
        """
        Main class function to start the project validation process.

        Check archive directory, load configuration file, synchronizes the
        remote repository with the local, parse updates and create new builds.
        """
        if self.project is None:
            LOG.error("Project with ID %s not found!", self.project)
            return
        new_archive = self.project.archive_path == ''
        # Check archive dir
        if new_archive:
            if not self._create_archive():
                return
        # Load config from yaml file
        config = self._load_config()
        if config is None:
            return
        # Create new build for new commits
        new_commits = self._check_update(new_archive=new_archive)
        for branch, commits in new_commits.items():
            for commit in commits:
                build_name = "{0}-{1}-{2}-{3}".format(self.project.name,
                                                      branch,
                                                      commit.hexsha[:7],
                                                      int(time.time()))
                self.sess.add(Build(build_name, self.project,
                                    config, commit.hexsha, branch,
                                    commit.author.name, commit.message))
                LOG.debug("Add build %s", build_name)
        # Update database
        self.sess.commit()
