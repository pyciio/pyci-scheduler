"""
Scheduler module. The main module of the project. Contains the daemon classes
for monitoring updates in projects and builds.
"""

import os
import time
import logging
import shutil
from multiprocessing import Process
from daemonize import Daemonize
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from rest.models.project import Project

from scheduler.config import Config
from scheduler.checker import ProjectChecker

LOG = logging.getLogger('pyci-scheduler')


class Scheduler(object):
    """
    Main scheduler class.

    In parallel, it launches the project scanner classes. A class can be run
    in two modes: normal (function run()) and as a daemon (function
    run_as_daemon()). For the configuration of the class, the Config class is
    used, similar to the Flask.Config class.
    """

    def __init__(self):
        self.config = Config()
        # Check debug env
        dev_mode = os.getenv('PYCI_DEV', False)
        if dev_mode:
            self.config.from_object('config.DevelopmentConfig')
        else:
            self.config.from_object('config.ProductionConfig')
        # Database connection
        db_uri = self.config.get('SQLALCHEMY_DATABASE_URI')
        self.db_engine = create_engine(db_uri,
                                       echo=False, convert_unicode=True)
        self.sess_maker = sessionmaker(bind=self.db_engine, autocommit=False)
        self.min_check_interval = 5

    def _checker(self, project_id):
        with ProjectChecker(self.db_engine, project_id,
                            self.config.get('REPO_ARCHIVE')) as checker:
            checker.run()

    def run(self):
        """
        Launching the project tracking process.
        """
        while True:
            try:
                start_time = int(time.time())
                sess = self.sess_maker()
                projects = sess.query(Project).all()
                # Start pool ProjectCheckers
                processes = list()
                for project in projects:
                    proc = Process(target=self._checker, args=(project.id,))
                    proc.start()
                # Wait checkers
                for proc in processes:
                    proc.join()  # pragma: no cover
                # Sleep if project check is fast
                run_time = int(time.time()) - start_time
                if run_time < self.min_check_interval:
                    time.sleep(self.min_check_interval - run_time)
            except KeyboardInterrupt:
                break
        LOG.debug("Exit......")

    def run_as_daemon(self, user, group, pid):
        """
        Launching the project tracking process in daemon mode.
        """
        # Get file descriptors which should not be closed
        keep_fds = []
        for hdlr in LOG.handlers:
            if isinstance(hdlr, logging.FileHandler):
                # pylint: disable=no-member
                keep_fds.append(hdlr.stream.fileno())
        # Daemonize
        daemon = Daemonize(app="pyci-scheduler",
                           pid=pid,
                           action=self.run,
                           keep_fds=keep_fds,
                           chdir=self.config.get('PROJECT_ROOT'),
                           logger=LOG,
                           user=user,
                           group=group)
        daemon.start()
