# PyCI #
[![Coverage Status](https://coveralls.io/repos/bitbucket/pyciio/pyci-scheduler/badge.svg?t=rsdgiM)](https://coveralls.io/bitbucket/pyciio/pyci-scheduler)

PyCI is a continuous integration tool written in python (version 2.7-3.5) using the Flask and SQLAlchemy (declarative ORM) frameworks. The assembly environment uses the docker.

It consists of three components:

  * **[pyci-scheduler](https://bitbucket.org/pyciio/pyci-scheduler)** - the main daemon that monitors updates in repositories and creates builds. The RESTful API server is part of the scheduler, JWT (algorithm RS512) tokens are used to authorize runners and users;
  * **[pyci-runner](https://bitbucket.org/pyciio/pyci-runner)** - a daemon for installing to third-party servers to run builds;
  * **[pyci-ui](https://bitbucket.org/pyciio/pyci-ui)** - web interface for managing CI, works also through API


![scheme](docs/scheme.png)

## Installation
### Install pyci-scheduler
From repository (recommended installation with a virtual environment)

```bash
git clone https://bitbucket.org/pyciio/pyci-scheduler
cd ./pyci-scheduler
# install requirements in virtualenv
virtualenv -p python3 env
./env/bin/pip install -r requirements.txt
```

Correct configuration file *./config.py* specify database settings in *ProductionConfig.SQLALCHEMY_DATABASE_URI*. To work with the database, the SQLAlchemy framework is used, so the project supports a large number of databases http://docs.sqlalchemy.org/en/latest/core/engines.html.

Install database scheme

```bash
# activate virtualenv
source ./env/bin/activate
# create database scheme
./scripts/db_manage.py create
# fill default data
./scripts/db_manage.py fill
```
*Other script options, see in "Database management"*

Generating keys for JWT

```bash
cd ./keys/jwt
openssl genrsa -out pyci.rsa 1024
openssl rsa -in pyci.rsa -pubout > pyci.rsa.pub
```

Generation Self-signed SSL certificate

```bash
cd ./keys/ssl
openssl genrsa -out server.key 2048
openssl ecparam -genkey -name secp384r1 -out server.key
openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650
```

### Install pyci-runner
Dependencies:

  * installed docker
  * access to the docker (the user from under which the runner is run must be in the docker group)

Install from repository (recommended installation with a virtual environment)

```bash
git clone https://bitbucket.org/pyciio/pyci-runner
cd ./pyci-runner
# install requirements in virtualenv
virtualenv -p python3 env
./env/bin/pip install -r requirements.txt
```

### Install pyci-ui

In future

## Running
### Run pyci-scheduler
```bash
cd <path_to_project>
source ./env/bin/activate
```

Run daemons (scheduler and api server)

```bash
# run scheduler
./scheduler.py -D -p /tmp/pyci-scheduler.pid -l pyci-scheduler.log
# run API server
gunicorn -D -b localhost:8080 -w 20 --certfile=keys/ssl/server.crt --keyfile=keys/ssl/server.key \
   --log-file pyci-scheduler-api.log --access-logfile pyci-scheduler-api.log rest:APP
```

### Run pyci-runner

Clone code from https://bitbucket.org/pyciio/pyci-runner

```bash
# register runner in project (this will create a configuration file with a token received from the PyCI scheduler)
./runner.py register -t ab75a9e94c6e4c4995cbf1db25fe73 -p project2
Register runner on server http://localhost:8080/api/1.0/register in project project2
Enter runner's name [mac-book-velp]: runner.local 
Enter runner's ip address [127.0.1.1]: 127.0.0.1
runner runner.local register

# start runner daemon
./runner.py start -D
```

See more options in help

```bash
$. /runner.py -h                                                    
usage: runner.py [ACTION] [OPTIONS]

optional arguments:
  -h, --help        - Print the help message

Available actions:
  {register,start}  - To get more information use runner.py [ACTION] -h
$ ./runner.py register -h
usage: runner.py register [OPTIONS]

optional arguments:
  -h, --help            show this help message and exit
  -s SCHEDULER, --scheduler SCHEDULER
                        - Scheduler server. [localhost]
  --scheduler_port SCHEDULER_PORT
                        - Scheduler server port. [8080]
  -t TOKEN, --token TOKEN
                        - Project token.
  -e {docker}, --executor {docker}
                        - Runner type.
  -p PROJECT, --project PROJECT
                        - Project name.
$ ./runner.py start -h   
usage: runner.py start [OPTIONS]

optional arguments:
  -h, --help            show this help message and exit
  -D, --daemon          - Daemonize the Scheduler process. [False]
  -l LOG, --log LOG     - The log file to write to. [-]
  --log-level LOG_LEVEL
                        - The granularity of log outputs. [info]
  -p PID, --pid PID     - A filename to use for the PID file. [None]
  -u USER, --user USER  - Switch worker processes to run as this user. [velp]
  -g GROUP, --group GROUP
                        - Switch worker process to run as this group. [velp]
  --max-workers MAX_WORKERS
                        - Max count parallel workers. [None]
```

### Run pyci-ui

In future

## Configure CI project
1) Create *[.pyci.yml](.pyci.yml)* file in root of project.

2) Get token
```bash
curl -H "Content-Type: application/json" \
     -X POST \
     -d '{"username":"admin", "password":"admin"}' \
     http://localhost:8080/api/1.0/authenticate
{
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJleHAiOjE0OTAwMTA5MjIsImlhdCI6MTQ5MDAwNzMyMiwiY2lkIjozLCJzY3AiOlsiYWRtaW4iXX0.D9NpbMlvNWVgaXkqLpfBrXcdH2M534sXbpAylZ9-7_XGawpNwUIpkxcSjesuxEtzUBlOwIqC_hW2SlyPjmO22Ts3JRIMXkefTrFXb_Hxv1AbFT6FM6VK4_k26nrcuiBfNHIrmof5CzlAl0fuZLGSMvdwPheQIRPwmNr1dJiPERI"
}
```

3) Create new team for project
```bash
curl -H "Content-Type: application/json" \
     -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJleHAi\
OjE0OTAwMTA5MjIsImlhdCI6MTQ5MDAwNzMyMiwiY2lkIjozLCJzY3AiOlsiYWRtaW4iXX0.D9Np\
bMlvNWVgaXkqLpfBrXcdH2M534sXbpAylZ9-7_XGawpNwUIpkxcSjesuxEtzUBlOwIqC_hW2SlyP\
jmO22Ts3JRIMXkefTrFXb_Hxv1AbFT6FM6VK4_k26nrcuiBfNHIrmof5CzlAl0fuZLGSMvdwPheQ\
IRPwmNr1dJiPERI" \
     -X POST \
     -d '{"name":"Test-team", "description":"Team for test-project"}' \
     http://localhost:8080/api/1.0/teams 
{
  "msg": "team Test-team create"
}
```

4) Create project
```bash
curl -H "Content-Type: application/json" \
     -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJleHAi\
OjE0OTAwMTA5MjIsImlhdCI6MTQ5MDAwNzMyMiwiY2lkIjozLCJzY3AiOlsiYWRtaW4iXX0.D9Np\
bMlvNWVgaXkqLpfBrXcdH2M534sXbpAylZ9-7_XGawpNwUIpkxcSjesuxEtzUBlOwIqC_hW2SlyP\
jmO22Ts3JRIMXkefTrFXb_Hxv1AbFT6FM6VK4_k26nrcuiBfNHIrmof5CzlAl0fuZLGSMvdwPheQ\
IRPwmNr1dJiPERI" \
     -X POST \
     -d '{"name":"test-project", "repository": "http://github.com/url_to_project", "team": "Test-team"}' \
     http://localhost:8080/api/1.0/projects
{
  "msg": "project test-project create"
}
```

5) Get project token for runners
```bash
curl -H "Content-Type: application/json" \
     -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJleHAi\
OjE0OTAwMTA5MjIsImlhdCI6MTQ5MDAwNzMyMiwiY2lkIjozLCJzY3AiOlsiYWRtaW4iXX0.D9Np\
bMlvNWVgaXkqLpfBrXcdH2M534sXbpAylZ9-7_XGawpNwUIpkxcSjesuxEtzUBlOwIqC_hW2SlyP\
jmO22Ts3JRIMXkefTrFXb_Hxv1AbFT6FM6VK4_k26nrcuiBfNHIrmof5CzlAl0fuZLGSMvdwPheQ\
IRPwmNr1dJiPERI" \
     -X GET \
     http://localhost:8080/api/1.0/projects
{
  "projects": [
    {
      "archive": "", 
      "create_datetime": "Mon, 18 Mar 2017 9:01:27 GMT", 
      "id": 1, 
      "members": [], 
      "name": "test-project", 
      "repository": "http://github.com/url_to_project", 
      "runners": [], 
      "team": "Test-team", 
      "token": "6d55e3bdaf710bb4449f5decbba68e"
    }
  ]
}
```

6) Use token *6d55e3bdaf710bb4449f5decbba68e* for register runner in project 
```bash
runner.py register
```

## Database management

Database repository is located in the directory *./db_repository*. To manage the database: create a schema, create a migration, upgrade the version and downgrade the script is used *./scripts/db_manage.py*.

```bash
./scripts/db_manage.py 
Usage: ./scripts/db_manage.py <action>

Actions:
    create    - create new database and version control
    downgrade - downgrade database version
    upgrade   - upgrade database version
    migrade   - migrade database
    fill      - fill database default data (roles)
```

To connect to the database, the script uses a common configuration *./config.py*

## Features
  * Works on python versions from 2.7 to 3.5.
  * Support for many types of databases (http://docs.sqlalchemy.org/en/latest/core/engines.html)
  * Database versioning and migration system
  * JWT tokens are used with the RS512 algorithm to authorize users and runners
  * 100% code coverage with unit tests for all python versions (used tox)
  * High linting code (pylint with all levels)
  * PEP8
  * Tools for deploying the development environment
  
## Develop

The project makefile is used to: prepare the development environment, run tests, analyze the coverage of tests, build the project in a package or docker image.

```bash
make help
    clean
        Remove build artifacts and packages.
    env
        Rebuild virtualenv and install requirements.
    package-ubuntu
        Build DEB packages.
    package-rpm
        Build RPM packages.
    packages
        Build all types packages.
    build-image
        Build docker image.
    lint
        Run code linting.
    tests
        Run unittests.
    coverage
        Calculate unittests coverage.
    coverage-report
        Calculate unittests coverage and open HTML report
```

### Prepare development enviroment

```bash
cd <path_to_project>
# creating an virtual environment with two major versions of the python (ptyhon2 in ./venv/py2, python3 in ./venv/py3)
make env
# enable the use of the configuration DevelopmentConfig (in ./config.py)
# When this mode is enabled, a temporary database sqlite3 is used
export PYCI_DEV=1
# activate virtual enviroment with python2
source ./venv/py2/bin/activate
# or activate virtual enviroment with python3
source ./venv/py3/bin/activate
```

### Run unit tests and linting

```bash
cd <path_to_project>
tox
```

### Run pyci-scheduler-api server in Dev mode

```
cd <path_to_project>
make env
source ./venv/py3/bin/activate
export PYCI_DEV=1
export FLASK_APP=rest/__init__.py
export FLASK_DEBUG=1
flask run --host localhost --port 8080 --reload
```

### Run pyci-scheduler in Dev mode

```
cd <path_to_project>
make env
source ./venv/py3/bin/activate
export PYCI_DEV=1
python ./scheduler.py
```

### Calculate coverage

```bash
make coverage
```

### Build packages (.deb and .rpm)

```bash
make packages
```

### Build docker image

```bash
make build-image
```
