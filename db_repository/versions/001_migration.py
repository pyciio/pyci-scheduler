# pylint: disable=wildcard-import,unused-wildcard-import
from sqlalchemy import *
from migrate import *
from migrate.changeset import schema
from rest.models.password import Password


pre_meta = MetaData()
post_meta = MetaData()
permissions = Table('permissions', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('role_id', Integer),
    Column('user_id', Integer),
    UniqueConstraint('role_id', 'user_id', name='_roles_uc'),
)

role = Table('role', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=20), nullable=False),
    Column('description', String(length=140)),
)

user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('login', String(length=64), nullable=False),
    Column('password', Password),
    Column('email', String(length=120), nullable=False),
    Column('status', Enum('verified', 'unverified')),
    Column('create', DateTime),
    Column('last_modified', DateTime),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['permissions'].create()
    post_meta.tables['role'].create()
    post_meta.tables['user'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['permissions'].drop()
    post_meta.tables['role'].drop()
    post_meta.tables['user'].drop()
