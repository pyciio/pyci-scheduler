# pylint: disable=wildcard-import,unused-wildcard-import
from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
permissions = Table('permissions', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('role_id', INTEGER),
    Column('user_id', INTEGER),
)

roles = Table('roles', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('role_id', Integer),
    Column('user_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['permissions'].drop()
    post_meta.tables['roles'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['permissions'].create()
    post_meta.tables['roles'].drop()
