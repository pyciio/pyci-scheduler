from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
build = Table('build', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=64), nullable=False),
    Column('config', Text),
    Column('logs', Text),
    Column('create', DateTime),
    Column('started', DateTime),
    Column('ended', DateTime),
    Column('status', Enum('successful', 'failed', 'stoped', 'wait', 'new', 'run')),
    Column('project_id', Integer, nullable=False),
    Column('commit_message', Text),
    Column('commit_hash', String(length=40)),
    Column('commit_branch', String(length=100)),
    Column('commit_author', String(length=64)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['build'].columns['commit_author'].create()
    post_meta.tables['build'].columns['commit_branch'].create()
    post_meta.tables['build'].columns['commit_hash'].create()
    post_meta.tables['build'].columns['commit_message'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['build'].columns['commit_author'].drop()
    post_meta.tables['build'].columns['commit_branch'].drop()
    post_meta.tables['build'].columns['commit_hash'].drop()
    post_meta.tables['build'].columns['commit_message'].drop()
