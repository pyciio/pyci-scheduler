# pylint: disable=wildcard-import,unused-wildcard-import
from sqlalchemy import *
from migrate import *
from migrate.changeset import schema
from rest.models.ipaddr import IPAddress


pre_meta = MetaData()
post_meta = MetaData()
runner = Table('runner', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=20), nullable=False),
    Column('create', DateTime),
    Column('ip_address', IPAddress, nullable=False),
    Column('executor', Enum('docker', 'shell')),
    Column('token', Text),
    Column('project_id', Integer, nullable=False),
    UniqueConstraint('name', 'project_id', name='_runner_uc'),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['runner'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['runner'].drop()
