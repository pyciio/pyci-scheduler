"""
Configuration file with global settings
"""

import os

# pylint: disable=too-few-public-methods


class Config(object):
    """Base configuration class with default parametrs"""
    # Stage option
    DEBUG = False
    TESTING = False
    # Base options
    PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

    # ==> Scheduler <===
    REPO_ARCHIVE = "/var/lib/pyci-scheduler"

    # ==> API server <==
    SERVER_NAME = "localhost:8080"
    JSON_AS_ASCII = False
    # SSL certs
    SSL_CRT = "keys/ssl/server.crt"
    SSL_KEY = "keys/ssl/server.key"
    # API JWT settings
    JWT_RSA_PUB = "keys/jwt/pyci.rsa.pub"
    JWT_RSA_PRIV = "keys/jwt/pyci.rsa"
    JWT_EXPIRE = 3600  # in seconds
    # API
    API_VERSION = 1.0
    API_PATH = "/api/%s" % API_VERSION

    # ==> Database <==
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(PROJECT_ROOT,
                                                          'app.db')
    SQLALCHEMY_MIGRATE_REPO = os.path.join(PROJECT_ROOT, 'db_repository')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False


class ProductionConfig(Config):
    """Configuration class for production"""
    # Create database in docker:
    # docker run --rm --name mysql-dev -e MYSQL_RANDOM_ROOT_PASSWORD=yes \
    # -e MYSQL_DATABASE=test -e MYSQL_USER=test -e MYSQL_PASSWORD=test \
    # -p 3306:3306 -d mysql:latest
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://test:test@localhost/test?" \
                              "host=localhost?port=3306"


class DevelopmentConfig(Config):
    """Configuration class for development"""
    DEBUG = True
    REPO_ARCHIVE = "/tmp/pyci-scheduler-archive"


class TestingConfig(Config):
    """Configuration class for testing"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    API_PATH = ""
