import logging
logging.basicConfig()
#logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
import rest.models as models

## CREATE
# Setup engine
engine = create_engine('sqlite:///:memory:', echo=True, convert_unicode=True)
# Create database scheme
models.Base.metadata.create_all(engine)


## CONNECT
# Create scoped session for .query property
session = scoped_session(sessionmaker(bind=engine, autocommit=False))
# setup session for use
models.Base.query = session.query_property()


## TESTS
# Delete users and roles
users = models.user.User.query.all()
for u in users:
    session.delete(u)
    print("======> Delete user:", u)
roles = models.role.Role.query.all()
for r in roles:
    session.delete(r)
    print("======> Delete role:", r)
session.commit()

# Create user
u = models.user.User('test', 'test', 'test@example.ru')
session.add(u)
# Create role
for i in range(7):
    r = models.role.Role(name='role%d' % i)
    session.add(r)
# Commit
session.commit()
for u in models.user.User.query.all():
    print("======> Create user:", u.id, u.login)
for r in models.role.Role.query.all():
    print("======> Create role:", r.id, r.name)

# Create roles
u = models.user.User.query.get(1)
roles = models.role.Role.query.limit(2).all()
for r in roles:
    p = models.role.Roles(user=u, role=r)
    session.add(p)
session.commit()

# Check
u = models.user.User.query.get(1)
print("======> User roles:", u.roles.all())
print("======> Check user password: ", u.password == "test")
r = models.role.Role.query.get(1)
print("======> Role back_ref:", r.roles.all())
