#!/bin/bash
PROJECT_DIR="/opt/pyci-scheduler"
# Init virtualenv
if [ ! -f "$PROJECT_DIR/venv/bin/python" ]; then
    virtualenv -p python3 $PROJECT_DIR/venv
fi
# Install requirements
if ls $PROJECT_DIR/requirements.txt 1> /dev/null 2>&1; then
    for req in `ls $PROJECT_DIR/requirements.txt`; do
        $PROJECT_DIR/venv/bin/pip install -r $req
    done
fi
# Create log directory
mkdir -p /var/log/pyci-scheduler
mkdir -p /var/log/pyci-scheduler-api