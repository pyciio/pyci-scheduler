#!/usr/bin/env python
"""
Modified database manager from Miguel flask tutorial:
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database
"""

import os
import sys
import imp
from migrate.versioning import api
from migrate.exceptions import DatabaseAlreadyControlledError
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
# Add parent directory to sys.path
sys.path.append(os.path.split(sys.path[0])[0])
from config import DevelopmentConfig, ProductionConfig
import rest.models as models

_help = '''Usage: {0} <action>

Actions:
    create    - create new database and version control
    downgrade - downgrade database version
    upgrade   - upgrade database version
    migrade   - migrade database
    fill      - fill database default data (roles)
'''


def create_db(uri, repo):
    engine = create_engine(uri, echo=False)
    # Create database scheme
    models.Base.metadata.create_all(engine)
    if not os.path.exists(repo):
        api.create(repo, 'PyCI database repository')
        api.version_control(uri, repo)
    else:
        api.version_control(uri, repo, api.version(repo))


def downgrade_db(uri, repo):
    version = api.db_version(uri, repo)
    api.downgrade(uri, repo, version - 1)
    new_version = api.db_version(uri, repo)
    print("Current database version: {0}".format(new_version))


def upgrade_db(uri, repo):
    api.upgrade(uri, repo)
    new_version = api.db_version(uri, repo)
    print("Current database version: {0}".format(new_version))


def migrate_db(uri, repo):
    new_version = api.db_version(uri, repo) + 1
    migration = "{0}/versions/{1:03d}_migration.py".format(repo, new_version)
    tmp_module = imp.new_module('old_model')
    old_model = api.create_model(uri, repo)
    exec(old_model, tmp_module.__dict__)
    script = api.make_update_script_for_model(uri, repo, tmp_module.meta,
                                              models.Base.metadata)
    open(migration, "wt").write(script)
    api.upgrade(uri, repo)
    print("New migration saved as {0}".format(migration))
    cur_version = api.db_version(uri, repo)
    print("Current database version: {0}".format(cur_version))


def fill_db(uri):
    engine = create_engine(uri, echo=False)
    session = sessionmaker(bind=engine)()
    session.add(models.role.Role('user'))
    session.add(models.role.Role('admin'))
    session.commit()


def fill_test_db(uri):
    engine = create_engine(uri, echo=False)
    session = sessionmaker(bind=engine)()
    # Roles
    role1 = models.role.Role('user')
    session.add(role1)
    role2 = models.role.Role('admin')
    session.add(role2)
    # Teams
    team1 = models.team.Team('team1')
    team2 = models.team.Team('team2')
    # Users
    user1 = models.user.User('fiona', 'password', 'fiona@example.com')
    user2 = models.user.User('scarlett', 'password', 'scarlett@example.com')
    user3 = models.user.User('admin', 'admin', 'admin@pyci.com')
    session.add(user1)
    session.add(user2)
    session.add(user3)
    # Projects
    session.add(models.project.Project('project1', 'https://github.com/velp/go-cfgreader', team1))
    session.add(models.project.Project('project2', 'https://github.com/velp/go-rknresolver', team2))
    # Relationships
    session.add(models.role.Roles(role=role1, user=user1))
    session.add(models.role.Roles(role=role1, user=user2))
    session.add(models.role.Roles(role=role2, user=user3))
    session.add(models.team.Teams(team=team1, user=user1))
    session.add(models.team.Teams(team=team1, user=user2))
    session.add(models.team.Teams(team=team2, user=user2))
    session.commit()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.stderr.write(_help.format(sys.argv[0]))
        sys.exit(1)
    else:
        # Init config
        dev_mode = os.getenv('PYCI_DEV', False)
        if dev_mode:
            print("Use development config for database:")
            cfg = DevelopmentConfig
        else:
            print("Use production config for database:")
            cfg = ProductionConfig
        db_uri = cfg.SQLALCHEMY_DATABASE_URI
        db_repo = cfg.SQLALCHEMY_MIGRATE_REPO
        print(db_uri)
        # Run functions
        if sys.argv[1] == "create":
            try:
                create_db(db_uri, db_repo)
            except DatabaseAlreadyControlledError:
                print("Database already create, see {0}".format(db_uri))
        elif sys.argv[1] == "downgrade":
            downgrade_db(db_uri, db_repo)
        elif sys.argv[1] == "upgrade":
            upgrade_db(db_uri, db_repo)
        elif sys.argv[1] == "migrade":
            migrate_db(db_uri, db_repo)
        elif sys.argv[1] == "fill":
            fill_db(db_uri)
        elif sys.argv[1] == "fill-test":
            fill_test_db(db_uri)
        else:
            print("Unrecognized action:", format(sys.argv[1]))
            sys.exit(1)
